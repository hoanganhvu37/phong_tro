@extends('chu_tro.master')
@section('content')
<div class="card ">
    <div class="card-body">
        <div>
            <ul  class="iq-edit-profile d-flex nav nav-pills">
                <li class="col-md-6 p-0" id="change_capnhap">
                   <a class="nav-link active" data-toggle="pill" >
                      Cập Nhật Thông Tin
                   </a>
                </li>
                <li class="col-md-6 p-0" id="change_password">
                    <a class="nav-link" data-toggle="pill" >
                       Đổi Mật Khẩu
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="iq-card" id="update_password">
    <div class="iq-card-header d-flex justify-content-between">
       <div class="iq-header-title">
          <h4 class="card-title">Đổi Mật Khẩu</h4>
       </div>
    </div>
    <div class="iq-card-body">
       <form>
          <div class="form-group">
             <label>New Password:</label>
             <input type="Password" class="form-control" id="password" value="">
          </div>
          <div class="form-group">
             <label>Verify Password:</label>
                <input type="Password" class="form-control" id="re_password" value="">
          </div>
          <button type="button" class="btn btn-primary mr-2" id="doimatkhau">Cập Nhật Mật Khẩu</button>
          <button type="button" class="btn iq-bg-danger">Cancle</button>
       </form>
    </div>
</div>

<input id="id" type="text" hidden>
<div class="tab-pane fade active show" id="update_capnhap" role="tabpanel">
    <div class="iq-card">
      <div class="iq-card-header d-flex justify-content-between">
         <div class="iq-header-title">
            <h4 class="card-title">Cập Nhật Thông Tin</h4>
         </div>
      </div>
      <div class="iq-card-body">
         <form>
            <div class="form-group row align-items-center">
               <div class="col-md-12">
                  <div class="profile-img-edit">
                    <div id="holder" style="max-height:100px;"></div>
                     <div class="p-image">
                       {{-- <i class="ri-pencil-line upload-button"></i> --}}
                       <a data-input="hinh_anh" id="lfm" data-preview="holder">
                            <i class="ri-pencil-line upload-button"></i>
                       </a>
                       <input hidden id="hinh_anh" class="form-control" type="text">
                    </div>
                  </div>
               </div>
            </div>

            <div id="test" class=" row align-items-center">
               <div class="form-group col-sm-6">
                  <label>Họ và Tên</label>
                  <input type="text" class="form-control" id="full_name" >
               </div>
               <div class="form-group col-sm-6">
                  <label>Email</label>
                  <input type="text" class="form-control" id="email">
               </div>
               <div class="form-group col-sm-6">
                  <label>Số Điện Thoại</label>
                  <input type="text" class="form-control" id="so_dien_thoai">
               </div>
               <div class="form-group col-sm-6">
                  <label>Ngày Sinh</label>
                  <input type="date" class="form-control" id="ngay_sinh">
               </div>
               <div class="form-group col-sm-6">
                <label>CMDN/CCCD</label>
                <input type="text" class="form-control" id="cmnd_cccd">
             </div>
               <div class="form-group col-sm-6">
                  <label>Giới Tính</label>
                  <select id="gioi_tinh" class="form-control">
                    <option value=1>Nam</option>
                    <option value=0>Nữ</option>
                </select>
               </div>
               <div class="form-group col-sm-12">
                  <label>Địa Chỉ</label>
                  <input type="text" class="form-control" id="dia_chi">
                  </textarea>
               </div>
            </div>
            <div class="text-right">
                <button type="button" id="updateChuTro" class="btn btn-primary mr-2">Cập Nhật</button>
                <button type="button" id="reset" class="btn iq-bg-danger">Cancle</button>
            </div>
         </form>
      </div>
   </div>
</div>
@endsection
@section('js')
<script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
<script>
    $('#lfm').filemanager('image', {prefix: '/laravel-filemanager'});
</script>
    <script>

        $(document).ready(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            loadInfo();
            function loadInfo() {
                $.ajax({
                    url     : '/chu-tro/us-chu-tro/edit',
                    type    : 'get',
                    success : function(res) {
                        var image = '<img src="' + res.data.hinh_anh +'" style="max-height:150px;border-radius:50%;">'
                        $("#email").val(res.data.email);
                        $("#full_name").val(res.data.ho_lot + ' ' + res.data.ten);
                        $("#so_dien_thoai").val(res.data.so_dien_thoai);
                        $("#dia_chi").val(res.data.dia_chi);
                        $("#ngay_sinh").val(res.data.ngay_sinh);
                        $("#gioi_tinh").val(res.data.gioi_tinh);
                        $("#cmnd_cccd").val(res.data.cmnd_cccd);
                        $("#hinh_anh").val(res.data.hinh_anh);
                        $("#holder").html(image);
                    },
                });
            };
            // loadtable();

            $('body').on('click', '.doiTrangThai', function() {
                console.log('click');
                var id = $(this).data('id');
                $.ajax({
                    url         : '/chu-tro/us-chu-tro/update-status/' + id,
                    type        : 'get',
                    success     : function(res) {
                        if (res.status) {
                            toastr.success("Đã cập nhật trạng thái chủ trọ !");
                            loadtable();
                        } else {
                            toastr.error("Lỗi!!");
                        }
                    },
                });
            });

            // $("#update_capnhap").hide();
            $("#update_password").hide();

            $("#change_capnhap").click(function(){
                $("#update_password").hide();
                $("#update_capnhap").show();
                $("#thongtin").hide();

            });

            // $("#change_thongtin").click(function(){
            // $("#update_capnhap").hide();
            //     $("#update_password").hide();
            //     $("#thongtin").show();

            // });

            $("#change_password").click(function(){
                $("#update_capnhap").hide();
                $("#thongtin").hide();
                $("#update_password").show();
            });

            $("#doimatkhau").click(function(){
                var payload = {
                    'password'          :        $("#password").val(),
                    're_password'       :        $("#re_password").val(),
                };
                $.ajax({
                    url :    '/chu-tro/us-chu-tro/change-password',
                    type :   'post',
                    data :      payload,
                    success :   function(res){
                        if(res.status){

                            toastr.success('Đã Đổi Mật Khẩu Thành Công');
                        }else{
                            toastr.error('Đã có lỗi xảy ra');
                        }
                    },
                    error   :   function(res) {
                        var listError = res.responseJSON.errors;
                        $.each(listError, function(key, value) {
                            toastr.error(value[0]);
                        });
                    },
                });
            });

            $("#updateChuTro").click(function(){
                console.log('click');
                var payload = {
                    "id"                   :      $("#id").val(),
                    "email"                :      $("#email").val(),
                    "full_name"            :      $("#full_name").val(),
                    "so_dien_thoai"        :      $("#so_dien_thoai").val(),
                    "dia_chi"              :      $("#dia_chi").val(),
                    "ngay_sinh"            :      $("#ngay_sinh").val(),
                    "gioi_tinh"            :      $("#gioi_tinh").val(),
                    "cmnd_cccd"            :      $("#cmnd_cccd").val(),
                    "hinh_anh"             :      $("#hinh_anh").val(),
                };
                console.log(payload);
                $.ajax({
                    url :    '/chu-tro/us-chu-tro/update',
                    type :   'post',
                    data :      payload,
                    success :   function(res){
                        if(res.status){
                            toastr.success('Đã Cập Nhật Thành Công');
                        } else{
                            toastr.error('Đã Cập Nhật Thành Công');
                        }
                        toastr.success('Đã Cập Nhật Thành Công');
                        // $("#update_capnhap").hide();
                        // $("#thongtin").show();
                        // loadtable();
                    },
                    error   :   function(res) {
                        var listError = res.responseJSON.errors;
                        $.each(listError, function(key, value) {
                            toastr.error(value[0]);
                        });
                    },
                });
            });

        });
    </script>
@endsection
