@extends('chu_tro.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="mb-3">
                                <label class="form-label">Mã Phòng Trọ</label>
                                <input tabindex="1" class="form-control " id="ma_phong_tro" name="ma_phong_tro" type="text"
                                    placeholder="Nhập vào mã phòng trọ">
                                    <small id="message_ma_phong_tro" class="">   </small>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="mb-3">
                                <label class="form-label">Tiêu Đề</label>
                                <input tabindex="2" class="form-control" id="tieu_de" name="tieu_de" type="text"
                                    placeholder="Nhập vào tiêu đề">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="mb-3">
                                <label class="form-label">Slug Tiêu Đề </label>
                                <input  class="form-control" id="slug_tieu_de" name="slug_tieu_de" type="text">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="mb-3">
                                <label class="form-label">Giá 1 tháng</label>
                                <input tabindex="3" class="form-control" id="gia_thang" name="gia_thang" type="number"
                                    placeholder="Nhập vào giá tháng phòng trọ">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="mb-3">
                                <label class="form-label">Số ngày đợi cọc</label>
                                <input tabindex="3" class="form-control" id="thoi_gian_coc" name="thoi_gian_coc" type="number"
                                    placeholder="Nhập vào số ngày đợi cọc">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="mb-3">
                                <label class="form-label">Số Phòng</label>
                                <input tabindex="5" class="form-control" id="so_phong" name="so_phong" type="text"
                                placeholder="Nhập vào số phòng trọ">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="mb-3">
                                <label class="form-label">Tình Trạng</label>
                                <select name="is_open" id="is_open" class="form-control">
                                    <option value=1>Hiển Thị</option>
                                    <option value=0>Tạm Tắt</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="mb-3">
                                <label class="form-label">Loại Phòng</label>
                                <select  name="id_loai" id="id_loai" class="form-control">
                                    <option value=0>Hiển Thị</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="mb-3">
                                <label class="form-label">Địa Chỉ Phòng Trọ</label>
                                <input tabindex="6" class="form-control" id="dia_chi_phong_tro" name="dia_chi_phong_tro" type="text"
                                placeholder="Nhập vào địa chỉ phòng trọ">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="mb-3">
                                <label class="form-label">Hình Ảnh</label>
                                <div class="input-group">
                                    <input type="text" id="thumbnail" class="form-control">
                                    <div class="input-group-append">
                                        <span class="input-group-text lfm bg-danger lfm" data-input="thumbnail" data-preview="holder">Choose</span>
                                     </div>
                                 </div>
                                 <div id="holder" style="margin-top:15px;max-height:100px;"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 mt-3">
                                <textarea tabindex="7" class="form-control" id="mo_ta_phong_tro" name="mo_ta_phong_tro" cols="30" rows="10" placeholder="Nhập vào mô tả ngắn phòng trọ"></textarea>
                            </div>
                            <div class="col-md-12 mt-3">
                                <textarea tabindex="8" class="form-control" id="mo_ta_chi_tiet" name="mo_ta_chi_tiet" cols="30" rows="10" placeholder="Nhập vào mô tả chi tiết"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <button id="createPhongTro" class="btn btn-primary" type="button">Thêm Mới Phòng Trọ</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <h4 class="ml-3">Danh Sách Phòng Trọ</h4>
                <div class="card-body">
                    <table class="table table-bordered" id="danhSachPhongTro">
                        <thead>
                            <tr class="text-center">
                                <th scope="col">#</th>
                                <th scope="col">Mã Phòng Trọ</th>
                                <th scope="col">Tiêu Đề</th>
                                <th scope="col">Loại Phòng</th>
                                <th scope="col">Giá 1 tháng</th>
                                <th scope="col">Địa Chỉ Phòng Trọ</th>
                                <th scope="col">Số Phòng</th>
                                <th scope="col">Tình Trạng</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
              <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <input type="text" hidden id="id_them_moi_phong_delete">
                <div class="alert alert-secondary" role="alert">
                    <h4 class="alert-heading">Xóa loại phòng!</h4>
                    <p>Bạn có chắc chắn muốn xóa loại phòng này không?.</p>
                    <hr>
                    <p class="mb-0"><i>Lưu ý: Hành động không thể khôi phục lại</i>.</p>
                </div>
            </div>
            <div class="modal-footer">
              <button class="btn btn-primary" type="button" data-dismiss="modal">Close</button>
              <button id="deleteThemMoiPhong" class="btn btn-secondary" type="button" data-dismiss="modal">Lưu</button>
            </div>
          </div>
        </div>
    </div>
    <div class="modal fade bd-example-modal-xl" id="editModal" tabindex="-1" role="dialog"   aria-hidden="true">
        <div class="modal-dialog modal-xl">
           <div class="modal-content">
              <div class="modal-header">
                 <h5 class="modal-title">Modal title</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                 <span aria-hidden="true">&times;</span>
                 </button>
              </div>
              <div class="modal-body">
                <input type="text" name="" id="id_them_moi_phong_edit" hidden>
                <div class="row">
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label class="form-label">Mã Phòng Trọ</label>
                            <input tabindex="1" class="form-control" id="ma_phong_tro_edit" name="ma_phong_tro" type="text"
                                placeholder="Nhập vào mã phòng trọ">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label class="form-label">Tiêu Đề</label>
                            <input tabindex="2" class="form-control" id="tieu_de_edit" name="tieu_de" type="text"
                                placeholder="Nhập vào tiêu đề">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label class="form-label">Slug Tiêu Đề </label>
                            <input  class="form-control" id="slug_tieu_de_edit" name="slug_tieu_de" type="text">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label class="form-label">Giá 1 tháng</label>
                            <input tabindex="3" class="form-control" id="gia_thang_edit" name="gia_thang" type="number"
                                placeholder="Nhập vào giá tháng phòng trọ">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label class="form-label">Số ngày đợi cọc</label>
                            <input tabindex="3" class="form-control" id="thoi_gian_coc_edit" name="thoi_gian_coc" type="number"
                                placeholder="Nhập vào số ngày đợi cọc">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label class="form-label">Số Phòng</label>
                            <input tabindex="5" class="form-control" id="so_phong_edit" name="so_phong" type="text"
                            placeholder="Nhập vào số phòng trọ">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label class="form-label">Tình Trạng</label>
                            <select id="is_open_edit" class="form-control">
                                <option value=1>Hiển Thị</option>
                                <option value=0>Tạm Tắt</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label class="form-label">Hình Ảnh</label>
                            <div class="input-group">
                                <input type="text" id="hinh_anh_edit" class="form-control">
                                <div class="input-group-append">
                                    <span class="input-group-text lfm bg-danger lfm" data-input="hinh_anh_edit" data-preview="edit_holder">Choose</span>
                                 </div>
                             </div>
                             <div id="edit_holder" style="margin-top:15px;max-height:100px;"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label class="form-label">Loại Phòng</label>
                            <select id="id_loai_edit" name="id_loai_edit" class="form-control">
                                <option value=0>Hiển Thị</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label class="form-label">Địa Chỉ Phòng Trọ</label>
                            <input tabindex="6" class="form-control" id="dia_chi_phong_tro_edit" name="dia_chi_phong_tro" type="text"
                            placeholder="Nhập vào địa chỉ phòng trọ">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 mt-3">
                        <textarea tabindex="7" class="form-control" id="mo_ta_phong_tro_edit" name="mo_ta_phong_tro" cols="30" rows="10" placeholder="Nhập vào mô tả ngắn phòng trọ"></textarea>
                    </div>
                    <div class="col-md-12 mt-3">
                        <textarea tabindex="8" class="form-control" id="mo_ta_chi_tiet_edit" name="mo_ta_chi_tiet" cols="30" rows="10" placeholder="Nhập vào mô tả chi tiết"></textarea>
                    </div>
                </div>

              </div>
              <div class="modal-footer ">
                 <button type="button" class="btn btn-secondary " data-dismiss="modal">Close</button>
                 <button type="button" id="updateThemMoiPhong" class="btn btn-primary " data-dismiss="modal">Save</button>
              </div>
           </div>
        </div>
     </div>
@endsection
@section('js')
    <script src="https://cdn.ckeditor.com/4.18.0/standard/ckeditor.js"></script>
    <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
    <script>
        $('.lfm').filemanager('image', {prefix: '/laravel-filemanager'});
        var options = {
        filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
        filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
        filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
        filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
    };
    </script>
    <script>
         $(document).ready(function() {
            CKEDITOR.replace('mo_ta_chi_tiet');
            CKEDITOR.replace('mo_ta_phong_tro');
            CKEDITOR.replace('mo_ta_chi_tiet_edit');
            CKEDITOR.replace('mo_ta_phong_tro_edit');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            loadLoaiPhong();
            loadTable();

            function converToSlug(str) {
                str = str.toLowerCase();

                str = str
                    .normalize('NFD') // chuyển chuỗi sang unicode tổ hợp
                    .replace(/[\u0300-\u036f]/g, ''); // xóa các ký tự dấu sau khi tách tổ hợp

                // Thay ký tự đĐ
                str = str.replace(/[đĐ]/g, 'd');

                // Xóa ký tự đặc biệt
                str = str.replace(/([^0-9a-z-\s])/g, '');

                // Xóa khoảng trắng thay bằng ký tự -
                str = str.replace(/(\s+)/g, '-');

                // Xóa ký tự - liên tiếp
                str = str.replace(/-+/g, '-');

                // xóa phần dư - ở đầu & cuối
                str = str.replace(/^-+|-+$/g, '');

                // return
                return str;
            };

            $("#tieu_de").keyup(function() {
                var slug = converToSlug($("#tieu_de").val());
                $("#slug_tieu_de").val(slug);
            });

            $("#tieu_de_edit").keyup(function() {
                var slug = converToSlug($("#tieu_de_edit").val());
                $("#slug_tieu_de_edit").val(slug);
            });

            $('#createPhongTro').click(function(){
                var payload = {
                    'ma_phong_tro'          : $('#ma_phong_tro').val(),
                    'tieu_de'               : $('#tieu_de').val(),
                    'slug_tieu_de'          : $('#slug_tieu_de').val(),
                    'id_loai'               : $('#id_loai').val(),
                    'gia_thang'             : $('#gia_thang').val(),
                    'gia_thang'             : $('#gia_thang').val(),
                    'dia_chi_phong_tro'     : $('#dia_chi_phong_tro').val(),
                    'thoi_gian_coc'         : $("#thoi_gian_coc").val(),
                    'ten_chu_tro'           : $('#ten_chu_tro').val(),
                    'hinh_anh'              : $('#thumbnail').val(),
                    'so_phong'              : $('#so_phong').val(),
                    'mo_ta_phong_tro'       : CKEDITOR.instances['mo_ta_phong_tro'].getData(),
                    'mo_ta_chi_tiet'        : CKEDITOR.instances['mo_ta_chi_tiet'].getData(),
                    'is_open'               : $('#is_open').val(),
                };
                console.log(payload);
                $.ajax({
                    url     :   '/chu-tro/phong/index',
                    type    :   'post',
                    data    :   payload,
                    success :   function(res) {
                        if(res.status){
                            toastr.success(res.mess);
                            loadTable();
                        }else{
                            toastr.error(res.mess);
                        }
                    },
                    error   : function(res) {
                        var listError = res.responseJSON.errors;
                        $.each(listError, function(key, value) {
                            toastr.error(value[0]);
                        });
                    },
                });
            });

            $('body').on('click', '.doiTrangThai', function() {
                var them_moi_phong_id = $(this).data('id');
                $.ajax({
                    url         : '/chu-tro/phong/update-status/' + them_moi_phong_id,
                    type        : 'get',
                    success     : function(res) {
                        if (res.status) {
                            toastr.success("Đã cập nhập trạng thái phòng trọ !");
                            loadTable();
                        } else {
                            toastr.error("Lỗi!!");
                        }
                    },
                });
            });

            $("#deleteThemMoiPhong").click(function() {
                var id = $("#id_them_moi_phong_delete").val();
                $.ajax({
                    url           : '/chu-tro/phong/destroy/' + id,
                    type          : 'get',
                    success       : function(res) {
                        if (res.status) {
                            toastr.success("Đã xóa phòng thành công !");
                            loadTable();
                        } else {
                            toastr.error("Phòng không tồn tại!");
                        }
                    },
                });
            });

            $('body').on('click', '.delete', function() {
                var id_them_moi_phong = $(this).data('id');
                $("#id_them_moi_phong_delete").val(id_them_moi_phong);
            });

            $('body').on('click', '.edit', function() {
                var id_them_moi_phong = $(this).data('id');
                console.log(id_them_moi_phong);
                $.ajax({
                    url             : '/chu-tro/phong/edit/' + id_them_moi_phong,
                    type            : 'get',
                    success         : function(res) {
                        if (res.status) {
                            $("#id_them_moi_phong_edit").val(res.data.id);
                            $("#ma_phong_tro_edit").val(res.data.ma_phong_tro);
                            $("#id_loai_edit").val(res.data.id_loai);
                            $("#tieu_de_edit").val(res.data.tieu_de);
                            $("#slug_tieu_de_edit").val(res.data.slug_tieu_de);
                            $("#dia_chi_phong_tro_edit").val(res.data.dia_chi_phong_tro);
                            $("#thoi_gian_coc_edit").val(res.data.thoi_gian_coc);
                            $("#so_phong_edit").val(res.data.so_phong);
                            $("#hinh_anh_edit").val(res.data.hinh_anh);
                            $("#gia_thang_edit").val(res.data.gia_thang);
                            // $("#mo_ta_chi_tiet_edit").val(res.data.mo_ta_chi_tiet);
                            // $("#mo_ta_phong_tro_edit").val(res.data.mo_ta_phong_tro);
                            CKEDITOR.instances['mo_ta_chi_tiet_edit'].setData(res.data.mo_ta_chi_tiet);
                            CKEDITOR.instances['mo_ta_phong_tro_edit'].setData(res.data.mo_ta_phong_tro);
                            $("#is_open_edit").val(res.data.is_open);
                        } else {
                            toastr.error('Có lỗi xảy ra !!!');
                        }
                    },
                });
            });

            $("#updateThemMoiPhong").click(function() {
                var themMoiPhong = {
                    'id'                    :               $("#id_them_moi_phong_edit").val(),
                    'id_loai'               :               $("#id_loai_edit").val(),
                    'ma_phong_tro'          :               $("#ma_phong_tro_edit").val(),
                    'tieu_de'               :               $("#tieu_de_edit").val(),
                    'slug_tieu_de'          :               $("#slug_tieu_de_edit").val(),
                    'dia_chi_phong_tro'     :               $("#dia_chi_phong_tro_edit").val(),
                    'id_chu_tro'            :               $("#id_chu_tro_edit").val(),
                    'so_phong'              :               $("#so_phong_edit").val(),
                    'hinh_anh'              :               $("#hinh_anh_edit").val(),
                    'gia_thang'             :               $("#gia_thang_edit").val(),
                    'thoi_gian_coc'         :               $("#thoi_gian_coc_edit").val(),
                    'mo_ta_chi_tiet'        :               CKEDITOR.instances['mo_ta_chi_tiet_edit'].getData(),
                    'mo_ta_phong_tro'       :               CKEDITOR.instances['mo_ta_phong_tro_edit'].getData(),
                    'is_open'               :               $("#is_open_edit").val(),
                };
                console.log(themMoiPhong);
                $.ajax({
                    url             : '/chu-tro/phong/update',
                    type            : 'post',
                    data            : themMoiPhong,
                    success         : function(res) {
                        toastr.success("Đã cập nhật phòng trọ thành công!");
                        loadTable();
                    },
                    error   : function(res) {
                        var listError = res.responseJSON.errors;
                        $.each(listError, function (key, value) {
                            toastr.error(value[0]);
                        });
                    },
                });
            });

            $("#ma_phong_tro").blur(function(){
                var payload = {
                    'ma_phong_tro'  :   $("#ma_phong_tro").val(),
                };
                $.ajax({
                    url     :   '/chu-tro/phong/check-product-id',
                    type    :   'post',
                    data    :   payload,
                    success :   function(res) {
                        if(res.status){
                            $("#message_ma_phong_tro").text("Mã Phòng Trọ Đã Tồn Tại");
                            $("#ma_phong_tro").removeClass("border border-primary");
                            $("#ma_phong_tro").addClass("border border-danger");
                            $("#message_ma_phong_tro").removeClass("text-primary");
                            $("#message_ma_phong_tro").addClass("text-danger")

                        }else{
                            $("#message_ma_phong_tro").text("Mã Phòng Trọ Có Thể Tạo");
                            $("#ma_phong_tro").removeClass("border border-danger");
                            $("#ma_phong_tro").addClass("border border-primary");
                            $("#message_ma_phong_tro").removeClass("text-danger");
                            $("#message_ma_phong_tro").addClass("text-primary")
                        }
                    },
                    error   : function(res) {
                        var listError = res.responseJSON.errors;
                        $.each(listError, function(key, value) {
                            toastr.error(value[0]);
                        });
                    },
                });
            });

            function formatNumber(number)
            {
                return new Intl.NumberFormat('vi-VI', { style: 'currency', currency: 'VND' }).format(number);
            }

            function toVietNam(num) {
                var t = [
                        "không",
                        "một",
                        "hai",
                        "ba",
                        "bốn",
                        "năm",
                        "sáu",
                        "bảy",
                        "tám",
                        "chín",
                    ],
                    r = function (r, n) {
                        var o = "",
                            a = Math.floor(r / 10),
                            e = r % 10;
                        return (
                            a > 1
                                ? ((o = " " + t[a] + " mươi"), 1 == e && (o += " mốt"))
                                : 1 == a
                                ? ((o = " mười"), 1 == e && (o += " một"))
                                : n && e > 0 && (o = " lẻ"),
                            5 == e && a >= 1
                                ? (o += " lăm")
                                : 4 == e && a >= 1
                                ? (o += " tư")
                                : (e > 1 || (1 == e && 0 == a)) && (o += " " + t[e]),
                            o
                        );
                    },
                    n = function (n, o) {
                        var a = "",
                            e = Math.floor(n / 100),
                            n = n % 100;
                        return (
                            o || e > 0
                                ? ((a = " " + t[e] + " trăm"), (a += r(n, !0)))
                                : (a = r(n, !1)),
                            a
                        );
                    },
                    o = function (t, r) {
                        var o = "",
                            a = Math.floor(t / 1e6),
                            t = t % 1e6;
                        a > 0 && ((o = n(a, r) + " triệu"), (r = !0));
                        var e = Math.floor(t / 1e3),
                            t = t % 1e3;
                        return (
                            e > 0 && ((o += n(e, r) + " ngàn"), (r = !0)),
                            t > 0 && (o += n(t, r)),
                            o
                        );
                    };

                if (0 == num) return t[0];
                var str = "",
                    a = "";
                do
                    (ty = num % 1e9),
                        (num = Math.floor(num / 1e9)),
                        (str = num > 0 ? o(ty, !0) + a + str : o(ty, !1) + a + str),
                        (a = " tỷ");
                while (num > 0);
                str = str.trim();

                return str.charAt(0).toUpperCase() + str.slice(1);
            };

            function loadLoaiPhong()
            {
                $.ajax({
                    url     :   '/chu-tro/phong/get-data',
                    type    :   'get',
                    success :   function(res) {
                        var contentLoaiPhong = '';
                        $.each(res.listLoaiPhong, function(key, value) {
                            contentLoaiPhong += '<option value='+ value.id +'>' + value.ten_loai_phong +'</option>'
                        });
                        $("#id_loai").html(contentLoaiPhong);
                        $("#id_loai_edit").html(contentLoaiPhong);
                    },
                });
            }

            function loadTable() {
                $.ajax({
                    url     :   '/chu-tro/phong/get-data',
                    type    :   'get',
                    success :   function(res) {
                        var phongTro = res.data;
                        var content = '';
                        $.each(phongTro, function(key, value) {
                            content +='<tr class="align-middle">';
                            content +='<th class="text-center">'+(key+1)+'</th>';
                            content +='<td class="text-center">'+value.ma_phong_tro+'</td>';
                            content +='<td class="text-center">'+value.tieu_de+'</td>';
                            content +='<td class="text-center">'+value.ten_loai_phong+'</td>';
                            content +='<td class="text-center">'+formatNumber(value.gia_thang)+'</td>';
                            content +='<td class="text-center">'+value.dia_chi_phong_tro+'</td>';
                            content +='<td class="text-center">'+value.so_phong+'</td>';
                            content +='<td class="text-center">';
                            if(value.is_open == 1) {
                                content += '<button class="btn btn-primary doiTrangThai" data-id="' + value.id + '">Hiển Thị</button>';
                            } else {
                                content += '<button class="btn btn-danger doiTrangThai" data-id="' + value.id + '">Tạm Tắt</button>';
                            }
                            content +='</td>';
                            content +='<td class="text-center text-nowrap">';
                            content +='<button class="btn btn-info edit" data-id="' + value.id + '" style ="margin-right : 10px;" data-toggle="modal" data-target="#editModal">Edit</button>';
                            content +='<button class="btn btn-danger delete" data-id="' + value.id + '" data-toggle="modal" data-target="#deleteModal">Delete</button>';
                            content +='</td>';
                            content +='</tr>';
                        });
                        $("#danhSachPhongTro tbody").html(content);
                    },
                });
            }
        });
    </script>
@endsection
