@extends('chu_tro.master')
@section('content')
<div class="card">
    <div class="card-body">
        <table class="table table-bordered">
            <thead>
                <tr>
                <th scope="col" class="text-center">STT</th>
                <th scope="col" class="text-center">Phòng Thuê</th>
                <th scope="col" class="text-center">Tên Người Thuê</th>
                <th scope="col" class="text-center">Email/Phone</th>
                <th scope="col" class="text-center">Giá Tháng</th>
                <th scope="col" class="text-center">Tiền Cọc</th>
                <th scope="col" class="text-center">Hình Ảnh</th>
                <th scope="col" class="text-center">Tình Trạng</th>
                </tr>
            </thead>
            <tbody>
                @if (isset($viewPayment))
                    @foreach ($viewPayment as $key => $value )
                        @php
                            $hinh_anh = explode(',',$value->hinh_anh)
                        @endphp
                        <tr>
                            <th scope="row" class="text-center">{{$key + 1}}</th>
                            <td class="text-center">{{$value->tieu_de}}</td>
                            <td class="text-center">{{$value->ho_lot}} {{$value->ten}}</td>
                            <td class="text-center">{{$value->email}} / {{$value->phone}}</td>
                            <td class="text-center">{{ number_format($value->gia_thang, 0) }} VNĐ</td>
                            <td class="text-center">{{ number_format($value->tien_coc, 0) }} VNĐ</td>
                            <td class="text-center">
                                <img src="{{$hinh_anh[0]}}" width="200px" >
                            </td>
                            @if ($value->id_done == 0)
                                <td class="align-middle text-center">
                                    <button class="btn btn-danger">Chưa Thanh Toán Cọc</button>
                                </td>
                            @else
                                <td class="align-middle text-center">
                                    <button class="btn btn-success" data-toggle="modal" data-target="#thongtinthanhtoan" data-id="{{$value->id}}">Đã Thanh Toán</button>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                @endif
            </tbody>
            </table>
    </div>
</div>
    <!-- Modal -->
<div class="modal fade" id="thongtinthanhtoan" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Thông Tin</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th scope="col" class="text-center">Ngày Thanh Toán</th>
                <th scope="col" class="text-center">Tiền Cọc Đã Thanh Toán</th>
              </tr>
            </thead>
            <tbody>
                <tr>
                  <td class="text-center">123123</td>
                  <td class="text-center">123123</td>
                </tr>
            </tbody>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
</div>
@endsection
@section('js')

@endsection
