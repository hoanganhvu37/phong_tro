<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Đăng nhập chủ trọ</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        .gradient-custom-3 {
        /* fallback for old browsers */
        background: #84fab0;

        /* Chrome 10-25, Safari 5.1-6 */
        background: -webkit-linear-gradient(to right, rgba(132, 250, 176, 0.5), rgba(143, 211, 244, 0.5));

        /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
        background: linear-gradient(to right, rgba(132, 250, 176, 0.5), rgba(143, 211, 244, 0.5))
        }
        .gradient-custom-4 {
        /* fallback for old browsers */
        background: #84fab0;

        /* Chrome 10-25, Safari 5.1-6 */
        background: -webkit-linear-gradient(to right, rgba(132, 250, 176, 1), rgba(143, 211, 244, 1));

        /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
        background: linear-gradient(to right, rgba(132, 250, 176, 1), rgba(143, 211, 244, 1))
        }
    </style>
    @toastr_css
</head>
<body>
   <!-- Section: Design Block -->
<section class="vh-100 bg-image"
   style="background-image: url('https://mdbcdn.b-cdn.net/img/Photos/new-templates/search-box/img4.webp');">
   <div class="mask d-flex align-items-center h-100 gradient-custom-3">
     <div class="container h-100">
       <div class="row d-flex justify-content-center align-items-center h-100">
         <div class="col-12 col-md-9 col-lg-7 col-xl-6">
           <div class="card" style="border-radius: 15px;">
             <div class="card-body p-5">
               <h2 class="text-uppercase text-center mb-5">Đăng Nhập Chủ Trọ</h2>

               <form>
                 <div class="form-outline mb-4">
                     <label class="form-label">Email</label>
                   <input type="text" id="email" class="form-control form-control-lg" />
                 </div>

                 <div class="form-outline mb-4">
                     <label class="form-label">Password</label>
                   <input type="password" id="password" class="form-control form-control-lg" />
                 </div>
                 <div class="d-flex justify-content-center">
                   <button type="button" id="login_chu_tro" class="btn btn-success btn-block btn-lg gradient-custom-4 text-body">Đăng Nhập</button>
                 </div>

                 <p class="text-center text-muted mt-5 mb-0">Chưa có tài khoản? <a href="/chu-tro/sign-up"
                     class="fw-bold text-body"><u>Đăng ký</u></a></p>

               </form>

             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
</section>
  <!-- Section: Design Block -->
</body>
@jquery
@toastr_js
@toastr_render
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-kjU+l4N0Yf4ZOJErLsIcvOU2qSb74wXpOhqTvwVx3OElZRweTnQ6d31fXEoRD1Jy" crossorigin="anonymous"></script>
<script>
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("#login_chu_tro").click(function() {
            var payload = {
                'email'  :   $("#email").val(),
                'password'  :   $("#password").val(),
            };
            console.log(payload);
            $.ajax({
                url     :   '/chu-tro/login',
                type    :   'post',
                data    :   payload,
                success :   function(res) {
                    if(res.status == 4) {
                        toastr.success('Đăng nhập thành công');
                        window.location.href = "/chu-tro/us-chu-tro/index";
                    } else if (res.status == 5) {
                        toastr.error('Tài khoản chưa được kích hoạt');
                    } else {
                        toastr.error('Đăng nhập thất bại!');
                    }
                },
                error   :   function(res) {
                    var listError = res.responseJSON.errors;
                    $.each(listError, function(key, value) {
                        toastr.error(value[0]);
                    });
                },
            });
        })


    });

  </script>
</html>
