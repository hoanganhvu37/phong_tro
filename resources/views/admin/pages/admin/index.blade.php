@extends('admin.master')
@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-12">
                    <div class="iq-card">
                        <form id="formCreate">
                            <div class="iq-card-header d-flex justify-content-between">
                                <div class="iq-header-title">
                                    <h4 class="card-title">Thêm mới Admin</h4>
                                </div>
                            </div>
                            <div class="iq-card-body">
                                <div class="form-group">
                                    <label>Mã ADMIN </label>
                                    <input type="text" class="form-control" id="ma_admin" name="ma_admin">
                                    <span id="message_ma_admin"></span>
                                </div>
                                <div class="form-group">
                                    <label>Họ Và Tên</label>
                                    <input type="text" class="form-control" id="full_name" name="full_name">
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control" id="email" name="email">
                                    <span id="message_email"></span>
                                </div>
                                <div class="form-group">
                                    <label>Số Điện Thoại</label>
                                    <input type="text" class="form-control" id="so_dien_thoai" name="so_dien_thoai">
                                    <span id="message_sdt"></span>
                                </div>
                                <div class="form-group">
                                    <label>Mật Khẩu</label>
                                    <input type="password" id="password" name="password" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Nhập Lại Mật Khẩu</label>
                                    <input type="password" id="re_password" name="re_password" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Giới Tính</label>
                                    <select class="form-control" name="gioi_tinh" id="gioi_tinh">
                                        <option value="1">Nam</option>
                                        <option value="0">Nữ</option>
                                        <option value="2">Không xác định</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Quyền Quản Trị</label>
                                    <select name="is_master" id="is_master" class="form-control">
                                        <option value="1">Admin Boss - Trùm</option>
                                        <option value="0">Admin Bình Thường</option>
                                    </select>
                                </div>
                                <div class="text-right">
                                    <button id="createadmin" type="button" class="btn btn-primary">Thêm Mới</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="iq-card">
                <div class="iq-card-header">
                    <h5 class="">Danh Sách Tài Khoản Admin</h5>
                </div>
                <div class="iq-card-body">
                    <div class="table-responsive">
                        <table id=danhSachAdmin class="table table-bordered ">
                            <thead>
                                <tr class="text-center">
                                    <th><b>#</b></th>
                                    <th><b>Mã ADMIN</b></th>
                                    <th><b>Họ Và Tên</b></th>
                                    <th><b>Email</b></th>
                                    <th><b>Số Điện Thoại</b></th>
                                    <th><b>Quyền Quản Trị</b></th>
                                    <th><b>Tinh Trạng</b></th>
                                    <th><b>Action</b></th>
                                </tr>
                            </thead>
                            <tbody>
                                {{-- <tr class="text-center">
                                    <th scope="col" class="text-center">1</th>
                                    <th>Mã nè</th>
                                    <th>Tên nè</th>
                                    <th>Email nè</th>
                                    <th>0905523543</th>
                                    <th>Quyền quản trị nè</th>
                                    <td>
                                        <button class="btn btn-primary">Hoạt Động</button>
                                    </td>
                                    <td>
                                        <button class="btn btn-info">Cập Nhật</button>
                                    </td>
                                </tr> --}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title text-white" id="exampleModalLabel">Chỉnh Sửa</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="text-white" aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="text" name="" id="id" hidden>
                    <div class="form-group">
                        <label>Mã ADMIN </label>
                        <input type="text" class="form-control" id="ma_admin_edit" name="ma_admin">
                    </div>
                    <div class="form-group">
                        <label>Họ Và Tên</label>
                        <input type="text" class="form-control" id="full_name_edit" name="full_name">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control" id="email_edit" name="email">
                        <span id="message_email"></span>
                    </div>
                    <div class="form-group">
                        <label>Số Điện Thoại</label>
                        <input type="text" class="form-control" id="so_dien_thoai_edit" name="so_dien_thoai">
                        <span id="message_sdt"></span>
                    </div>
                    <div class="form-group">
                        <label>Mật Khẩu</label>
                        <input type="password" id="password_edit" name="password" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Nhập Lại Mật Khẩu</label>
                        <input type="password" id="re_password_edit" name="re_password" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Giới Tính</label>
                        <select class="form-control" name="gioi_tinh" id="gioi_tinh_edit">
                            <option value="1">Nam</option>
                            <option value="0">Nữ</option>
                            <option value="2">Không xác định</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Trạng Thái</label>
                        <select name="is_block" id="is_block_edit" class="form-control">
                            <option value="1">Đã Khóa</option>
                            <option value="0">Hoạt Động</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Quyền Quản Trị</label>
                        <select name="is_master" id="is_master_edit" class="form-control">
                            <option value="1">Admin Boss - Trùm</option>
                            <option value="0">Admin Bình Thường</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="button" data-dismiss="modal">Close</button>
                    <button id="updateAccount" class="btn btn-secondary" type="button" data-dismiss="modal">Save
                        changes</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title text-white" id="exampleModalLabel">Chỉnh Sửa</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="text-white" aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="text" name="" id="id_admin_delete" hidden>
                    <div class="alert alert-secondary" role="alert">
                        <div class="iq-alert-icon">
                            <i class="ri-information-line"></i>
                        </div>
                        <div class="iq-alert-text">Bạn có chắc chắc muốn <b>xóa</b> tài khoản này không?</div>

                    </div>
                    <div>
                        <h5>Lưu ý không thể khôi phục sau khi xóa</h5>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="button" data-dismiss="modal">Close</button>
                    <button id="deleteAccount" class="btn btn-secondary" type="button" data-dismiss="modal">Delete</button>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).ready(function() {

            $("#email").blur(function() {
                var payload = {
                    'email': $("#email").val(),
                };
                $.ajax({
                    url: '/admin/account/check-email',
                    type: 'post',
                    data: payload,
                    success: function(res) {
                        if (res.status == true) {
                            $("#message_email").html(
                                '<span class="text-danger">Email đã tồn tại</span>');
                        } else if (res.status == 3) {
                            $("#message_email").html(
                                '<span class="text-warning">Vui lòng điền vào Email</span>');
                        } else {
                            $("#message_email").html(
                                '<span class="text-primary">Email có thể được sử dụng</span>'
                                );
                        }
                    },
                    error: function(res) {
                        var listError = res.responseJSON.errors;
                        $.each(listError, function(key, value) {
                            toastr.error(value[0]);
                        });
                    },
                });
            });

            $("#so_dien_thoai").blur(function() {
                var payload = {
                    'so_dien_thoai': $("#so_dien_thoai").val(),
                };
                $.ajax({
                    url: '/admin/account/sdt',
                    type: 'post',
                    data: payload,
                    success: function(res) {
                        if (res.status) {
                            $("#message_sdt").html(
                                '<span class="text-danger">Số điện thoại đã tồn tại</span>');
                        } else if (res.status == 3) {
                            $("#message_sdt").html(
                                '<span class="text-warning">Vui lòng điền vào Số Điện Thoại</span>');
                        } else {
                            $("#message_sdt").html(
                                '<span class="text-primary">Số Điện Thoại có thể được sử dụng</span>'
                                );
                        }
                    },
                    error: function(res) {
                        var listError = res.responseJSON.errors;
                        $.each(listError, function(key, value) {
                            toastr.error(value[0]);
                        });
                    },
                });
            });

            $("#ma_admin").blur(function() {
                var payload = {
                    'ma_admin': $("#ma_admin").val(),
                };
                $.ajax({
                    url: '/admin/account/ma-admin',
                    type: 'post',
                    data: payload,
                    success: function(res) {
                        if (res.status) {
                            $("#message_ma_admin").html(
                                '<span class="text-danger">Mã Admin đã tồn tại</span>');
                        } else {
                            $("#message_ma_admin").html(
                                '<span class="text-primary">Mã Admin có thể được sử dụng</span>'
                                );
                        }
                    },
                    error: function(res) {
                        var listError = res.responseJSON.errors;
                        $.each(listError, function(key, value) {
                            toastr.error(value[0]);
                        });
                    },
                });
            });

            $("#createadmin").click(function() {
                console.log('Đã click');
                var payload = {
                    'email': $("#email").val(),
                    'ma_admin': $("#ma_admin").val(),
                    'full_name': $("#full_name").val(),
                    'so_dien_thoai': $("#so_dien_thoai").val(),
                    'gioi_tinh': $("#gioi_tinh").val(),
                    'is_master': $("#is_master").val(),
                    'password': $("#password").val(),
                    're_password': $("#re_password").val(),
                };

                $.ajax({
                    url: '/admin/account/index',
                    type: 'post',
                    data: payload,
                    success: function(res) {
                        toastr.success("Đã thêm mới tài khoản Admin thành công");
                        loadtable();
                        $("#formCreate").trigger("reset");
                    },
                    error: function(res) {
                        var listError = res.responseJSON.errors;
                        $.each(listError, function(key, value) {
                            toastr.error(value[0]);
                        });
                    },
                });
            });

            $('body').on('click', '.doiTrangThai', function() {
                var id = $(this).data('id');
                $.ajax({
                    url: '/admin/account/update-status/' + id,
                    type: 'get',
                    success: function(res) {
                        if (res.status) {
                            toastr.success("Đã cập nhật !");
                            loadtable();
                        } else {
                            toastr.warning(res.message);
                        }
                    },
                });
            });

            loadtable();

            $("body").on('click', '.edit', function() {
                var id = $(this).data('id');
                $.ajax({
                    url: '/admin/account/edit/' + id,
                    type: 'get',
                    success: function(res) {
                        if (res.status) {
                            $("#id").val(res.data.id);
                            $("#ma_admin_edit").val(res.data.ma_admin);
                            $("#email_edit").val(res.data.email);
                            $("#full_name_edit").val(res.data.ho_lot + ' ' + res.data.ten);
                            $("#so_dien_thoai_edit").val(res.data.so_dien_thoai);
                            $("#gioi_tinh_edit").val(res.data.gioi_tinh);
                            $("#is_master_edit").val(res.data.is_master);
                            $("#is_block_edit").val(res.data.is_block);

                        } else {
                            toastr.error("Tài khoản không tồn tại!");
                        }
                    },
                });
            });

            $("#updateAccount").click(function() {
                var payload = {
                    "id": $("#id").val(),
                    "ma_admin": $("#ma_admin_edit").val(),
                    "email": $("#email_edit").val(),
                    "full_name": $("#full_name_edit").val(),
                    "so_dien_thoai": $("#so_dien_thoai_edit").val(),
                    "gioi_tinh": $("#gioi_tinh_edit").val(),
                    "is_master": $("#is_master_edit").val(),
                    "is_block": $("#is_block_edit").val(),
                };

                $.ajax({
                    url: '/admin/account/update',
                    type: 'post',
                    data: payload,
                    success: function(res) {
                        if (res.status) {
                            toastr.success('Đã cập nhập');
                            loadtable();
                        } else {
                            loadtable();
                            toastr.warning(res.message);
                        }
                    },
                    error: function(res) {
                        var listError = res.responseJSON.errors;
                        $.each(listError, function(key, value) {
                            toastr.error(value[0]);
                        });
                    },
                });
            });
            $("#deleteAccount").click(function() {
                var id = $("#id_admin_delete").val();
                console.log(id);
                $.ajax({
                    url     :   '/admin/account/destroy/' + id,
                    type    :   'get',
                    success :   function(res) {
                        if(res.status) {
                            toastr.success("Đã xóa Admin thành công!");
                            loadtable();
                        } else {
                            toastr.error("Admin không tồn tại!");
                        }
                    },
                });
            });
            $("body").on('click', '.delete', function(){
                var id_admin = $(this).data('id');
                console.log(id_admin);
                $("#id_admin_delete").val(id_admin);
            });
            function loadtable() {
                $.ajax({
                    url: '/admin/account/get-data',
                    type: 'get',
                    success: function(res) {
                        var content = '';
                        $.each(res.data, function(key, value) {
                            if (value.is_master == 1) {
                                var Loai = "Admin Trùm";
                            } else {
                                var Loai = "Admin Thường"
                            }

                            content += '<tr class="text-center">';
                            content += '<td class="text-center">' + (key + 1) + '</td>';
                            content += '<td>' + value.ma_admin + '</td>';
                            content += '<td>' + value.ho_lot + ' ' + value.ten + '</td>';
                            content += '<td>' + value.email + '</td>';
                            content += '<td>' + value.so_dien_thoai + '</td>';
                            content += '<td>' + Loai + '</td>';
                            content += '<td>';
                            if (value.is_block == 1) {
                                content +=
                                    '<button class="btn btn-danger doiTrangThai text-nowrap" data-id="' +
                                    value.id + '">Đã Khóa</button>';
                            } else {
                                content +=
                                    '<button class="btn btn-primary doiTrangThai text-nowrap" data-id="' +
                                    value.id + '">Hoạt Động</button>';
                            }
                            content += '</td>';
                            content += '<td>';
                            content +=
                                '<button class="btn btn-info edit text-nowrap" data-id="' +
                                value.id +
                                '" " style="margin-right: 10px"  data-toggle="modal" data-target="#editModal">Cập Nhật</button>';
                            content +=
                                '<button class="btn btn-danger delete text-nowrap" data-id="' +
                                value.id +
                                '" style="margin-right: 10px" data-toggle="modal" data-target="#deleteModal">Xóa</button>';
                            content += '</td>';
                            content += '</tr>';
                        });
                        $("#danhSachAdmin tbody").html(content);
                    }
                });
            }
        });
    </script>
@endsection
