@extends('admin.master')
@section('content')
<div class="row">
    <div class="col-md-4">
            <div class="iq-card">
                <div class="iq-card-header d-flex justify-content-between">
                   <div class="iq-header-title">
                      <h4 class="card-title">Thêm Mới Loại Phòng Trọ</h4>
                   </div>
                </div>
                <div class="iq-card-body">
                    <form id="create" class="form theme-form">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Tên Loại Phòng</label>
                                        <input class="form-control" id="ten_loai_phong" name="ten_loai_phong" type="text"
                                            placeholder="Nhập vào tên loại phòng">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Slug Loại Phòng</label>
                                        <input class="form-control" id="slug_loai_phong" name="slug_loai_phong" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Tình Trạng</label>
                                        <select name="is_open" id="is_open" class="form-control">
                                            <option value=1>Hiển Thị</option>
                                            <option value=0>Tạm Tắt</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-right">
                            <button id="createLoaiPhong" class="btn btn-primary btn-lg" type="button">Thêm</button>
                            <input class="btn btn-info btn-lg" type="reset" value="Cancel">
                        </div>
                    </form>
                </div>
             </div>
    </div>
    <div class="col-md-8">
        <div class="iq-card">
            <div class="iq-card-header">
                <h5>Danh Sách Loại Phòng</h5>
            </div>
            <div class="iq-card-body">
                <table class="table table-bordered" id="listLoaiPhong">
                    <thead>
                       <tr class="text-center align-middle">
                          <th><strong>#</strong></th>
                          <th><strong>Tên loại Phòng </strong></th>
                          <th><strong>Tình Trạng</strong></th>
                          <th><strong>Action</strong></th>
                       </tr>
                    </thead>
                    <tbody>

                    </tbody>


                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white" id="exampleModalLabel">Chỉnh Sửa</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span class="text-white" aria-hidden="true">×</span>
            </button>
        </div>
        <div class="modal-body">
            <input type="text" name="" id="id_loai_phong_edit" hidden>
            <div class="mb-3">
                <label class="form-label">Tên Loại Phòng</label>
                <input class="form-control" id="ten_loai_phong_edit" name="ten_danh_muc" type="text"
                    placeholder="Nhập vào tên loại phòng">
            </div>
            <div class="mb-3">
                <label class="form-label">Slug Loại Phòng</label>
                <input class="form-control" id="slug_loai_phong_edit" name="slug_loai_phong" type="text"
                    placeholder="Nhập vào slug loại phòng">
            </div>
            <div class="mb-3">
                <label class="form-label">Tình Trạng</label>
                <select name="is_open" id="is_open_edit" class="form-control">
                    <option value=1>Hiển Thị</option>
                    <option value=0>Tạm Tắt</option>
                </select>
            </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-primary" type="button" data-dismiss="modal">Close</button>
          <button id="updateLoaiPhong" class="btn btn-secondary" type="button" data-dismiss="modal">Save changes</button>
        </div>
      </div>
    </div>
</div>
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-danger">
          <h5 class="modal-title text-white" id="exampleModalLabel">Xóa Loại Phòng</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span class="text-white" aria-hidden="true">×</span>
            </button>
        </div>
        <div class="modal-body">
            <input type="text" id="id_loai_phong_delete" hidden>
            <div class="alert text-white bg-danger" role="alert">
                <div class="iq-alert-icon">
                   <i class="ri-information-line"></i>
                </div>
                <div class="iq-alert-text">
                    <p>Bạn có chắc chắn muốn xóa loại phòng này không?.</p>
                <p class="mb-0"><i>Lưu ý: Hành động không thể khôi phục lại</i>.</p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-primary" type="button" data-dismiss="modal">Close</button>
          <button id="deleteLoaiPhong" class="btn btn-secondary" type="button" data-dismiss="modal">Lưu</button>
        </div>
      </div>
    </div>
</div>
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            function converToSlug(str) {
                str = str.toLowerCase();

                str = str
                    .normalize('NFD') // chuyển chuỗi sang unicode tổ hợp
                    .replace(/[\u0300-\u036f]/g, ''); // xóa các ký tự dấu sau khi tách tổ hợp

                // Thay ký tự đĐ
                str = str.replace(/[đĐ]/g, 'd');

                // Xóa ký tự đặc biệt
                str = str.replace(/([^0-9a-z-\s])/g, '');

                // Xóa khoảng trắng thay bằng ký tự -
                str = str.replace(/(\s+)/g, '-');

                // Xóa ký tự - liên tiếp
                str = str.replace(/-+/g, '-');

                // xóa phần dư - ở đầu & cuối
                str = str.replace(/^-+|-+$/g, '');

                // return
                return str;
            };

            $("#ten_loai_phong").keyup(function() {
                var slug = converToSlug($("#ten_loai_phong").val());
                $("#slug_loai_phong").val(slug);
            });

            $("#ten_loai_phong_edit").keyup(function() {
                var slug = converToSlug($("#ten_loai_phong_edit").val());
                $("#slug_loai_phong_edit").val(slug);
            });

            $("#createLoaiPhong").click(function(){
                var loaiPhong = {
                    'ten_loai_phong'                : $("#ten_loai_phong").val(),
                    'slug_loai_phong'               : $("#slug_loai_phong").val(),
                    'is_open'                       : $("#is_open").val(),
                };

                $.ajax({
                    url     :   '/admin/loai-phong/index',
                    type    :   'post',
                    data    :   loaiPhong,
                    success :  function(res) {
                        if(res.status) {
                            toastr.success('Đã thêm mới danh mục thành công!');
                            $('#create').trigger("reset");
                            loadTable();
                        }
                    },
                    error   :   function(res) {
                        var listError = res.responseJSON.errors;
                        $.each(listError, function(key, value) {
                            toastr.error(value[0]);
                        });
                    },
                });
            });

            $('body').on('click', '.doiTrangThai', function() {
                var loai_phong_id = $(this).data('id');
                $.ajax({
                    url         : '/admin/loai-phong/update-status/' + loai_phong_id,
                    type        : 'get',
                    success     : function(res) {
                        if (res.status) {
                            toastr.success("Đã cập trạng thái nhật danh mục!");
                            loadTable();
                        } else {
                            toastr.error("Lỗi!!");
                        }
                    },
                });
            });

            $('body').on('click', '.edit', function() {
                var id_loai_phong = $(this).data('id');
                $.ajax({
                    url             : '/admin/loai-phong/edit/' + id_loai_phong,
                    type            : 'get',
                    success         : function(res) {
                        if (res.status) {
                            $("#id_loai_phong_edit").val(res.data.id);
                            $("#ten_loai_phong_edit").val(res.data.ten_loai_phong);
                            $("#slug_loai_phong_edit").val(res.data.slug_loai_phong);
                            $("#is_open_edit").val(res.data.is_open);
                        } else {
                            toastr.error('Có lỗi xảy ra !!!');
                        }
                    },
                });
            });

            $("#updateLoaiPhong").click(function() {
                var loaiPhong = {
                    'id'                            : $("#id_loai_phong_edit").val(),
                    'ten_loai_phong'                : $("#ten_loai_phong_edit").val(),
                    'slug_loai_phong'               : $("#slug_loai_phong_edit").val(),
                    'is_open'                       : $("#is_open_edit").val(),
                };
                $.ajax({
                    url             : '/admin/loai-phong/update',
                    type            : 'post',
                    data            : loaiPhong,
                    success         : function(res) {
                        toastr.success("Đã cập nhật loại phòng thành công!");
                        loadTable();
                    },
                    error   : function(res) {
                        var listError = res.responseJSON.errors;
                        $.each(listError, function (key, value) {
                            toastr.error(value[0]);
                            // console.log(value[0]);
                        });
                    },
                });
            });

            $("#deleteLoaiPhong").click(function() {
                var id = $("#id_loai_phong_delete").val();
                $.ajax({
                    url           : '/admin/loai-phong/destroy/' + id,
                    type          : 'get',
                    success       : function(res) {
                        if (res.status) {
                            toastr.success("Đã xóa danh mục thành công !");
                            loadTable();
                        } else {
                            toastr.error("Danh mục không tồn tại!");
                        }
                    },
                });
            });

            $('body').on('click', '.delete', function() {
                var id_loai_phong = $(this).data('id');
                $("#id_loai_phong_delete").val(id_loai_phong);
            });


            loadTable();
            function loadTable() {
                $.ajax({
                    url     :   '/admin/loai-phong/get-data',
                    type    :   'get',
                    success :   function(res) {
                        var loaiPhong = res.data; // 1 array
                        var contentTable = '';
                        $.each(loaiPhong, function(key, value) {
                            contentTable += '<tr>';
                            contentTable += '<th class="text-center align-middle">'+ (key + 1) +'</th>';
                            contentTable += '<td class="text-nowrap align-middle">'+ value.ten_loai_phong +'</td>';
                            contentTable += '<td class="text-nowrap text-center align-middle">';
                            if(value.is_open == 1) {
                                contentTable += '<button class="btn btn-success doiTrangThai align-middle btn-block rounded-pill" data-id="' + value.id + '">Hiển Thị</button>';
                            } else {
                                contentTable += '<button class="btn btn-danger doiTrangThai btn-block rounded-pill" data-id="' + value.id + '">Tạm Tắt</button>';
                            }
                            contentTable += '</td>';
                            contentTable += '<td class="text-nowrap text-center align-middle">';
                            contentTable += '<button class="btn btn-info btn-lg edit align-middle rounded-pill mr-1" data-id="' + value.id + '" data-toggle="modal" data-target="#editModal">Edit</button>';
                            contentTable += '<button class="btn btn-danger btn-lg delete align-middle rounded-pill" data-id="' + value.id + '" data-toggle="modal" data-target="#deleteModal">Delete</button>';
                            contentTable += '</td>';
                            contentTable += '</tr>';
                        });
                        $("#listLoaiPhong tbody").html(contentTable);
                    },
                });
            }
        });
    </script>
@endsection
