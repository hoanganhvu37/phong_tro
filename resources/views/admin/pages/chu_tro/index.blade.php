@extends('admin.master')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                Quản Lý Chủ Trọ
            </div>
            <div class="card-body">
                <table  id="dansachChuTro" class="table table-bordered">
                    <thead>
                        <tr class="text-center">
                            <th>#</th>
                            <th>Họ Và Tên</th>
                            <th>Địa Chỉ Email</th>
                            <th>CMND/CCCD</th>
                            <th>Số Điện Thoại</th>
                            <th>Địa Chỉ</th>
                            <th>Tình Trạng</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{-- <tr class="text-center">
                            <th>1</th>
                            <th>fghfghfgh</th>
                            <th>fghfghfgh</th>
                            <th>fghfghfgh</th>
                            <th>fghfghfgh</th>
                            <th>fghfghfgh</th>
                            <th>fghfghfgh</th>
                            <th>fghfghfgh</th>
                            <td>
                                <button class="btn btn-primary">Hoạt Động</button>
                            </td>
                        </tr> --}}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
    <script>
        $(document).ready(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            loadtable();

            $('body').on('click', '.doiTrangThai', function() {
                console.log('click');
                var id = $(this).data('id');
                $.ajax({
                    url         : '/admin/chu-tro/update-status/' + id,
                    type        : 'get',
                    success     : function(res) {
                        if (res.status) {
                            toastr.success("Đã cập nhập trạng thái chủ trọ !");
                            loadtable();
                        } else {
                            toastr.error("Lỗi!!");
                        }
                    },
                });
            });

            $('body').on('click', '.delete', function() {
                console.log('click');
                var id = $(this).data('id');
                console.log('asdasd '+ id);
                $.ajax({
                    url         : '/admin/chu-tro/destroy/' + id,
                    type        : 'get',
                    success     : function(res) {
                        if (res.status) {
                            toastr.success("Đã xóa chủ trọ !");
                            loadtable();
                        } else {
                            toastr.error("Lỗi!!");
                        }
                    },
                });
            });

            function loadtable(){
                $.ajax({
                    url     :   '/admin/chu-tro/get-data',
                    type    :   'get',
                    success :   function(res) {
                        var content = '';
                        $.each(res.data, function(key, value) {
                            content += '<tr class="text-center">';
                            content += '<th>'+(key + 1)+'</th>';
                            content += '<th>'+ value.ho_lot + ' ' + value.ten +'</th>';
                            content += '<th>'+ value.email +'</th>';
                            content += '<th>'+ value.cmnd_cccd +'</th>';
                            content += '<th>'+ value.so_dien_thoai +'</th>';
                            content += '<th>'+ value.dia_chi +'</th>';
                            content += '<td>';
                            if(value.is_block==0){
                                content += '<button class="btn btn-primary doiTrangThai" data-id="' + value.id + '">Hoạt Động</button>';
                            }else{
                                content += '<button class="btn btn-danger doiTrangThai" data-id="' + value.id + '">Đã Khóa</button>';
                            }
                            content += '</td>';
                            content += '<td>';
                            content += '<button class="btn btn-danger delete" data-id="' + value.id + '">Xóa</button>';
                            content += '</td>';
                            content += '</tr>';
                        });
                        $("#dansachChuTro tbody").html(content);
                    },
                });
            }


        });
    </script>
@endsection
