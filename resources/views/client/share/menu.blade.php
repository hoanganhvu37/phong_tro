<header class="main-header style-two">
    <div class="header-top">
        <div class="auto-container">
            <div class="top-inner clearfix">
                <div class="top-left pull-left">
                    <ul class="info clearfix">
                        <li><i class="flaticon-email"></i><a href="mailto:support@example.com">support@example.com</a></li>
                        <li><i class="flaticon-global"></i> Kleine Pierbard 8-6 2249 KV Vries</li>
                    </ul>
                    <ul class="social-links clearfix">
                        <li><a href="index.html"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="index.html"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="index.html"><i class="fab fa-vimeo-v"></i></a></li>
                        <li><a href="index.html"><i class="fab fa-google-plus-g"></i></a></li>
                    </ul>
                </div>
                @if(Auth::guard('customer')->check())
                    <div class="btn btn-danger" id="tencustomer">
                        <div class="lang-btn">
                            <span class="txt">Chào bạn, {{ Auth::guard('customer')->user()->ho_lot . ' ' . Auth::guard('customer')->user()->ten}}</span>
                        </div>
                    </div>
                    <div class="btn btn-info">
                        <div class="lang-btn">
                            <a href="/customer/logout">
                                <span class="txt text-white">Đăng Xuất</span>
                            </a>

                        </div>
                    </div>
                @else
                <div class="top-right pull-right">
                    <div class="btn btn-danger" id="btndangnhap">
                        <div class="lang-btn">
                            <a href="/customer/view-login"><span class="txt text-white">Đăng Nhập</span></a>
                        </div>
                    </div>
                    <div class="btn btn-info" id="btndangky">
                        <div class="lang-btn" >
                            <a href="/customer/view-signup"><span class="txt text-white">Đăng Ký</span></a>
                        </div>
                    </div>
                </div>
                @endif

            </div>
        </div>
    </div>
    <div class="header-upper">
        <div class="auto-container">
            <div class="upper-inner">
                <figure class="logo-box"><a href="index.html"><img src="assets_client/images/logo.png" alt=""></a></figure>
                <div class="search-info">
                    <div class="select-box">
                        <select class="wide">
                           <option data-display="All Categories">All Categories</option>
                        </select>
                    </div>
                    <form class="search-form">
                        <div class="form-group">
                            <input type="search" id="searchPhongtro" placeholder="Nhập trọ cần tìm kiếm" >
                            <button type="submit"><i class="flaticon-search"></i></button>
                                <table class="table table-bordered" id="danhSachPhongTro">
                                    <tbody>

                                    </tbody>
                                </table>
                        </div>
                    </form>
                </div>

                <div class="menu-right-content clearfix">
                    @if (Auth::guard('customer')->check())
                        <button class="form-control btn-danger">
                            <li><a href="/customer/favorite"><i class="flaticon-like text-white"></i></a></li>
                        </button>
                    @else
                        <button class="form-control btn-danger" data-toggle="modal" data-target="#dangnhap">
                            <li><a href="#"><i class="flaticon-like text-white"></i></a></li>
                        </button>
                    @endif
                    @if (Auth::guard('customer')->check())
                        <button  class="form-control btn-warning mt-2">
                            <li><a href="/customer/payment-status"><i class="flaticon-user"></i></a></li>
                        </button>
                    @else
                        <button  class="form-control btn-warning mt-2" data-toggle="modal" data-target="#dangnhap">
                            <li><a href="#"><i class="flaticon-user"></i></a></li>
                        </button>
                    @endif


                </div>
            </div>
        </div>
    </div>
    <div class="header-lower">
        <div class="auto-container">
            <div class="outer-box clearfix">
                <div class="category-box pull-left">
                    <p>All Categories</p>
                    <ul class="category-content">
                        <li class="dropdown-option"><i class="flaticon-dress"></i>
                            <a href="index-2.html">Women’s Clothing</a>
                            <ul>
                               <li><a href="index-2.html">Categories 01</a></li>
                               <li><a href="index-2.html">Categories 02</a></li>
                               <li><a href="index-2.html">Categories 03</a></li>
                               <li><a href="index-2.html">Categories 04</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="menu-area pull-left">
                    <!--Mobile Navigation Toggler-->
                    <div class="mobile-nav-toggler">
                        <i class="icon-bar"></i>
                        <i class="icon-bar"></i>
                        <i class="icon-bar"></i>
                    </div>
                    <nav class="main-menu navbar-expand-md navbar-light">
                        <div class="collapse navbar-collapse show clearfix" id="navbarSupportedContent">
                            <ul class="navigation clearfix">
                                <li class="current dropdown"><a href="/">Home</a></li>
                                @if (isset($loaiPhong))
                                @foreach ($loaiPhong as $key => $value )
                                <li class="dropdown"><a href="/customer/category/{{$value->id}}">{{$value->ten_loai_phong}}</a></li>
                                @endforeach
                                @endif
                                <li><a href="#">Contact</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <!--sticky Header-->
    <div class="sticky-header">
        <div class="auto-container">
            <div class="outer-box clearfix">
                <div class="logo-box pull-left">
                    <figure class="logo"><a href="index.html"><img src="assets_client/images/small-logo.png" alt=""></a></figure>
                </div>
                <div class="menu-area pull-right">
                    <nav class="main-menu clearfix">
                        <!--Keep This Empty / Menu will come through Javascript-->
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>
{{-- <div class="modal fade" id="dangky" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-info">
          <h5 class="modal-title text-white" id="exampleModalLabel">Đăng Ký</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="dangkyCustomer">
                <div class="form-group">
                  <label for="exampleInputEmail1">Email</label>
                  <input type="email" class="form-control" name="email" >
                </div>
                <div class="form-group">
                  <label>Họ Và Tên</label>
                  <input type="text" class="form-control" name="full_name">
                </div>
                <div class="form-group">
                    <label>Mật Khẩu</label>
                    <input type="password" class="form-control" name="password">
                  </div>
                <div class="form-group">
                    <label>Nhập Lại Mật Khẩu</label>
                    <input type="password" class="form-control" name="re_password">
                </div>
                <div class="form-group">
                    <label>Số Điện Thoại</label>
                    <input type="number" class="form-control" name="phone">
                </div>
                <div class="form-group">
                    <label>Giới Tính</label>
                     <select name="" id="" class="form-control"></select>
                </div>
                <div class="form-group">
                    <label>Ngày Sinh</label>
                    <input type="date" class="form-control" name="dob">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn  btn-info">Đăng Ký</button>
                </div>
                <select name="" id="" class="form-control"></select>
            </form>
        </div>
      </div>
    </div>
</div>--}}
<div class="modal fade" id="dangnhap" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-danger">
          <h5 class="modal-title text-white" id="exampleModalLabel">Đăng Nhập</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="loginCustomerModal">
                <div class="form-group">
                  <label>Email</label>
                  <input type="email" class="form-control" name="email">
                </div>
                <div class="form-group">
                    <label>Mật Khẩu</label>
                    <input type="password" class="form-control" name="password">
                </div>
                <div class="form-group">
                    {!! NoCaptcha::renderJs() !!}
                    {!! NoCaptcha::display() !!}
                </div>
                <span class="text-center">{{ $errors->first('g-recaptcha-response') }}</span>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn  btn-info">Đăng Nhập</button>
                </div>
            </form>
        </div>
      </div>
    </div>
</div>
