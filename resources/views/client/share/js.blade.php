@jquery
@toastr_js
@toastr_render
<script src="/assets_client/js/jquery.js"></script>
<script src="/assets_client/js/popper.min.js"></script>
<script src="/assets_client/js/bootstrap.min.js"></script>
<script src="/assets_client/js/owl.js"></script>
<script src="/assets_client/js/wow.js"></script>
<script src="/assets_client/js/validation.js"></script>
<script src="/assets_client/js/jquery.fancybox.js"></script>
<script src="/assets_client/js/TweenMax.min.js"></script>
<script src="/assets_client/js/appear.js"></script>
<script src="/assets_client/js/scrollbar.js"></script>
<script src="/assets_client/js/jquery.nice-select.min.js"></script>
<script src="/assets_client/js/isotope.js"></script>
<script src="/assets_client/js/jquery-ui.js"></script>
<script src="/assets_client/js/jquery.bootstrap-touchspin.js"></script>
<script src="/assets_client/js/bxslider.js"></script>
<!-- main-js -->
<script src="/assets_client/js/script.js"></script>
