@extends('client.master')
@section('content')
<section class="page-title centred mt-3">
    <div class="pattern-layer" style="background-image: url(assets/images/background/page-title.jpg);"></div>
    <div class="auto-container">
        <div class="content-box">
            <h1>Yêu Thích</h1>
            <ul class="bread-crumb clearfix">
                <li><a href="/">Home</a></li>
            </ul>
        </div>
    </div>
</section>
<div class="container">
    @if (Auth::guard('customer')->check())
        @if (isset($list))
            @foreach ($list as $value )
            <div class="row mt-3">
                @php
                    $hinh_anh = explode(',', $value->hinh_anh )
                @endphp
                <div class="col-md-4">
                    <img src="{{$hinh_anh[0]}}">
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <div class="product-info">
                            <a href="javascript:;">
                                <h2 class="product-name mb-2"><b>{{$value->tieu_de}}</b></h2>
                            </a>
                            <h3>
                                <p>{!!$value->mo_ta_chi_tiet!!}</p>
                            </h3>
                            <div class="d-flex align-items-center">
                                <div class="mb-1 product-price"> <span class="me-1 text-decoration-line-through"><b>{{ number_format($value->gia_thang, 0) }} VNĐ</b></span></div>
                            </div>
                            <div class="product-action mt-2">
                                <div class="d-flex gap-2">
                                    <a href="/customer/product/{{$value->id_phong}}">
                                        <div class="btn btn-dark">Xem Chi Tiết..</div>
                                    </a>
                                    <div class="btn btn-danger ml-2 deleteYeuThich" data-id="{{$value->id_phong}}">Hủy Yêu Thích
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        @endif
    @endif
</div>
@endsection
@section('js')
@endsection
