@extends('client.master')
@section('content')
<section class="page-title centred mt-3">
    <div class="pattern-layer" style="background-image: url(assets/images/background/page-title.jpg);"></div>
    <div class="auto-container">
        <div class="content-box">
            <h1>Trạng Thái Thanh Toán</h1>
            <ul class="bread-crumb clearfix">
                <li></i><a href="/">Home</a></li>
            </ul>
        </div>
    </div>
</section>
<section class="cart-section cart-page">
    <div class="auto-container">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 table-column">
                <div class="table-outer">
                    <table class="cart-table">
                        <thead class="cart-header">
                            <tr>
                                <th>Action</th>
                                <th>&nbsp;</th>
                                <th class="prod-column">Tên Phòng</th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                                <th class="price">Giá Thuê</th>
                                <th class="price">Tiền Cọc</th>
                                <th>Trạng Thái</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (isset($sttPayment))
                                @foreach ($sttPayment as $key => $value)
                                <tr>
                                    <td class="align-middle text-center">
                                        <button class="btn btn-danger deleteSttPayment" {{$value->id_done == 1 ? 'hidden' : ''}} data-id="{{$value->id}}" >Hủy</button>
                                    </td>
                                    <td colspan="4" class="prod-column">
                                        <div class="column-box">
                                            @php
                                                $hinh_anh = explode(',', $value->hinh_anh )
                                            @endphp
                                            <div class="prod-thumb">
                                                <a href="/customer/product/{{$value->id}}"><img src="{{$hinh_anh[0]}}"></a>
                                            </div>
                                            <div class="prod-title">
                                                {{$value->tieu_de}}<nav></nav>
                                            </div>

                                        </div>
                                    </td>
                                    <td class="price">{{ number_format($value->gia_thang, 0) }} VNĐ</td>
                                    <td class="price">{{ number_format($value->tien_coc, 0) }} VNĐ</td>

                                    <td class="align-middle text-center">
                                        @if ($value->id_done == 0)
                                            <button class="btn btn-danger" data-toggle="modal" data-target="#thanhtoancoc">Chưa Thanh Toán Cọc</button>
                                        @else
                                            <button class="btn btn-success" data-toggle="modal" data-target="#dathanhtoan">Đã Thanh Toán</button>
                                        @endif

                                    </td>
                                </tr>
                                <div class="modal fade" id="dathanhtoan" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                          </button>
                                        </div>
                                        <div class="modal-body">
                                          <p>Vui lòng tới nhận trọ trong vòng {{$value->thoi_gian_coc}} ngày kể từ lúc thanh toán</p><br>
                                          <p>Nếu không bạn sẽ mất cọc !!!</p><br>
                                          <p>Xin cảm ơn !</p>
                                        </div>
                                        <div class="modal-footer">
                                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                          <button type="button" class="btn btn-primary">Save changes</button>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="thanhtoancoc" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Chuyển Khoản Tới Tài Khoản VCB : Vo Dinh Quoc Huy</p><br>
          <p>Số Tài Khoản: xxxxxxxxxxxx</p><br>
          <p>Để hoàn thành đặt cọc !</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
</div>

<!-- Modal -->
@endsection
@section('js')

@endsection
