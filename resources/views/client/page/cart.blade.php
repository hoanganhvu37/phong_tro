@extends('client.master')
@section('content')
<section class="page-title centred mt-3">
    <div class="pattern-layer" style="background-image: url(assets/images/background/page-title.jpg);"></div>
    <div class="auto-container">
        <div class="content-box">
            <h1>Trang Thanh Toán</h1>
            <ul class="bread-crumb clearfix">
                <li><i class="flaticon-home-1"></i><a href="index.html">Home</a></li>
            </ul>
        </div>
    </div>
</section>
<section class="checkout-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12 left-column">
                <div class="inner-box">
                    <div class="billing-info">
                        <h4 class="sub-title">Thông Tin</h4>
                        <form class="billing-form" id="formThue">
                            <div class="row">
                                <input type="text" name="id_customer" value="{{$customer->id}}" hidden>
                                <input type="text" name="id_phong" value="{{$phongTro->id}}" hidden>
                                <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                    <label>Họ và Tên<span class="text-danger">*</span></label>
                                    <div class="field-input">
                                        <input type="text" id="full_name"  class="form-control" value="{{$customer->ho_lot}} {{$customer->ten}}">
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                    <label>Email<span class="text-danger">*</span></label>
                                    <div class="field-input">
                                        <input type="email" id="email" class="form-control" value="{{$customer->email}}">
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                    <label>Địa Chỉ Hiện Tại<span class="text-danger">*</span></label>
                                    <div class="field-input">
                                        <input type="text" name="address" class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                    <label>Phone <span class="text-danger">*</span></label>
                                    <div class="field-input">
                                        <input type="number" id="phone" value="{{$customer->phone}}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mt-4">
                                <button class="btn btn-dark" type="submit">Thanh Toán Cọc</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 right-column">
                <div class="inner-box">
                    <div class="order-info">
                        <h4 class="sub-title">Phòng Trọ</h4>
                        <div class="order-product">
                            <ul class="order-list clearfix">
                                @php
                                    $hinh_anh = explode(',', $phongTro->hinh_anh)
                                @endphp
                                <li class="title clearfix">
                                    <p>{{$phongTro->ten_loai_phong}}</p>
                                    <span>Giá Thuê</span>
                                </li>
                                <li>
                                    <div class="single-box clearfix">
                                        <img src="{{$hinh_anh[0]}}" alt="">
                                        <h6><a href="/customer/product/{{$phongTro->id}}">{{$phongTro->tieu_de}}</a></h6>
                                        <span>{{ number_format($phongTro->gia_thang, 0) }} VNĐ</span>
                                    </div>
                                </li>
                                <li class="order-total clearfix">
                                    <h6>Tổng Tiền</h6>
                                    <span>{{ number_format($phongTro->gia_thang, 0) }} VNĐ</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('js')
@endsection
