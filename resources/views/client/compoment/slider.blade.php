<section class="banner-style-two">
    <div class="auto-container">
        <div class="banner-carousel-2 owl-carousel owl-theme owl-nav-none">
            @if (isset($phongTro))
                @foreach ($phongTro as $key => $value )
                @php
                    $hinh_anh = explode(',', $value->hinh_anh)
                @endphp
                    <div class="content-box" style="background-image: url({{$hinh_anh[0]}});">
                        <div class="inner-box">
                            <h1>{{$value->tieu_de}}</h1>
                            <p>{!!$value->mo_ta_chi_tiet!!}</p>
                            <a href="/customer/product/{{$value->id}}" class="theme-btn-two">Xem Chi Tiết<i class="flaticon-right-1"></i></a>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</section>
