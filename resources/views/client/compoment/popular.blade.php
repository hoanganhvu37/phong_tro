<div class="col-12 col-xl-3">
    <div class="bestseller-list mb-3 border p-3 bg-dark-1">
        <h6 class="mb-3 text-uppercase">Xem Nhiều Nhất</h6>
        <hr>
        @if (isset($hinh_anhs))
        @foreach ($phongTro as $key => $value)
        @php
            $hinh_anhs = explode(',', $value->hinh_anh)
        @endphp
            <div class="d-flex align-items-center">
                <div class="bottom-product-img">
                    <a href="/customer/product/{{$value->id}}">
                        <img src="{{$hinh_anhs[0]}}" width="80" class="mr-2">
                    </a>
                </div>
                <div class="ms-0">
                    <a href="/customer/product/{{$value->id}}">
                        <h6 class="mb-0 fw-light mb-1">{{$value->tieu_de}}</h6>
                    </a>
                    <p class="mb-0 text-white">{{$value->gia_thang}}</p>
                </div>
            </div>
            <hr>
        @endforeach
    </div>
    <div class="card rounded-0 w-100">
        <img src="{{$hinh_anhs[0]}}" class="card-img h-100" >
        <div class="card-img-overlay text-center top-20">
            <div class="border border-white border-3 py-3 bg-dark-3">
                <h5 class="card-title">Fashion Summer Sale</h5>
                <p class="card-text text-uppercase fs-1 text-white lh-1 mt-3 mb-2">Up to 80% off</p>
                <p class="card-text fs-5">On top Fashion Brands</p>	<a href="javascript:;" class="btn btn-white btn-ecomm">SHOP BY FASHION</a>
            </div>
        </div>
    </div>
    @endif
</div>
