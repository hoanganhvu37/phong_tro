<div class="product-wrapper">
    <div class="row">
        @if (isset($listPhongTro))
            @foreach ($listPhongTro as $key => $value)
            @php
                $hinh_anhs = explode(',', $value->hinh_anh)
            @endphp
            <div class="col-md-12 mb-5">
                <div class="product-grid">
                    <div class="card rounded-0 product-card">
                        <div class="row g-0">
                            <div class="col-md-4">
                                <img src="{{$hinh_anhs[0]}}" class="img-fluid" alt="...">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <div class="product-info">
                                        <a href="javascript:;">
                                            <h2 class="product-name mb-2"><b>{{$value->tieu_de}}</b></h2>
                                        </a>
                                        <h3>{!!$value->mo_ta_chi_tiet!!}</h3>
                                        <div class="d-flex align-items-center">
                                            <div class="mb-1 product-price"> <span class="me-1 text-decoration-line-through"><b>{{ number_format($value->gia_thang, 0) }} VNĐ</b></span></div>
                                        </div>
                                        <div class="product-action mt-2">
                                            <div class="d-flex gap-2">
                                                <a href="/customer/product/{{$value->id}}">
                                                    <div class="btn btn-dark" >Xem Chi Tiết..</div>
                                                </a>
                                                @if (Auth::guard('customer')->check())
                                                <div class="btn btn-danger ml-2 yeuThich" data-id="{{$value->id}}" >Yêu Thích </div>
                                                @else
                                                <div class="btn btn-danger ml-2" data-toggle="modal" data-target="#dangnhap" >Yêu Thích </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        @endif
    </div>
    <!--end-->
</div>
