<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
@include('client.share.head')
</head>

<body class="stretched">

	<div id="wrapper" class="clearfix">

		@include('client.share.menu')

		<section id="content">
            <div class="content-wrap">
                <div class="container clearfix">
                    @yield('content')
                </div>
            </div>
		</section>

		@include('client.share.foot')

	</div>
	    <div id="gotoTop" class="icon-angle-up"></div>
    @yield('js')
    <script src="/js/app.js"></script>
	@include('client.share.js')
    <script>
        $(document).ready(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#dangkyCustomer').submit(function(e){
                console.log('đã click');
                e.preventDefault();
                var payload = window.getFormData($(this));
                console.log(payload);

                axios
                .post('/customer/sign-up', payload)
                .then((res) => {
                    if(res.data.status) {
                        toastr.success("Đã tạo tài khoản thành công!");
                        setTimeout(() => {
                            window.location.href = '/customer/view-login';
                        }, 900);
                    }
                })
                .catch((res) => {
                    var listError = res.response.data.errors;
                    $.each(listError, function(key, value) {
                        toastr.error(value[0]);
                    });
                });
            });

            $("#loginCustomer").submit(function(e) {
                console.log('click');
                e.preventDefault();
                var payload = window.getFormData($(this));
                console.log(payload);

                axios
                    .post('/customer/login', payload)
                    .then((res) => {
                    if(res.data.status == 4) {
                        toastr.success('Đăng nhập thành công');
                        window.location.href = "/";
                    } else if (res.data.status == 5) {
                        toastr.error('Tài khoản chưa được kích hoạt');
                    } else {
                        toastr.error('Thất bại! Tài khoản hoặc mật khẩu không đúng');
                    }
                    })
                    .catch((res) => {
                        var listError = res.responseJSON.data.errors;
                        $.each(listError, function(key, value) {
                            toastr.error(value[0]);
                        });
                    });
            });

            $("#loginCustomerModal").submit(function(e) {
                console.log('click');
                e.preventDefault();
                var payload = window.getFormData($(this));
                console.log(payload);

                axios
                    .post('/customer/login', payload)
                    .then((res) => {
                    if(res.data.status == 4) {
                        toastr.success('Đăng nhập thành công');
                        window.location.href = "/";
                        // $("#btndangnhap").hide();
                        // $("#btndangky").hide();
                        // $("#logout").show();
                        // $("#tencustomer").show();
                    } else if (res.data.status == 5) {
                        toastr.error('Tài khoản chưa được kích hoạt');
                    } else {
                        toastr.error('Đăng nhập thất bại!');
                    }
                    })
                    .catch((res) => {
                        var listError = res.responseJSON.data.errors;
                        $.each(listError, function(key, value) {
                            toastr.error(value[0]);
                        });
                    });
            });

            $(".yeuThich").click(function(e){
                console.log('sss');
                var id_phong = $(this).data('id');
                $.ajax({
                    url         : '/customer/add-yeu-thich/' + id_phong,
                    type        : 'get',
                    success     : function(res) {
                        if (res.status == 0) {
                            toastr.success(res.message);
                        } else if(res.status == 1) {
                            toastr.error(res.message);
                        } else {
                            toastr.error(res.message);
                        }
                    },
                });
            });

            $(".deleteYeuThich").click(function(){
                console.log('haha');
                var id_phong = $(this).data('id');

                $.ajax({
                    url         : '/customer/delete-yeu-thich/' + id_phong,
                    type        : 'get',
                    success     : function(res) {
                        if (res.status) {
                            toastr.success(res.message);
                            window.location.href = '/customer/favorite';
                        } else {
                            toastr.error(res.message);
                        }
                    },
                });
            });

            $("#formThue").submit(function(e){
                e.preventDefault();
                var payload = window.getFormData($(this));
                console.log(payload);
                axios
                .post('/customer/bill-detail', payload)
                .then((res) => {
                    if(res.data.status){
                        toastr.success('Thành Công');
                        window.location.href = "/customer/payment-status";
                    }else{
                        toastr.warning('Có phòng trọ bạn vẫn chưa thanh toán cọc !!!');
                    }
                })
                .catch((res) => {
                    var listError = res.response.data.errors;
                    $.each(listError, function(key, value) {
                        toastr.error(value[0]);
                    });
                });
            });

            $(".deleteSttPayment").click(function(){
                console.log('do day roi');
                var id_payment = $(this).data('id');
                console.log('ádasd'+ id_payment);
                $.ajax({
                    url         : '/customer/delete-payment-status/' + id_payment,
                    type        : 'get',
                    success     : function(res) {
                        if (res.status) {
                            toastr.success('Đã hủy phòng');
                            window.location.href = '/customer/cart/' + id_payment;
                        } else {
                            toastr.error('Có lỗi xảy ra');
                        }
                    },
                });
            });

            function viewPhongTro(list)
            {
                var content_phong_tro ='';
                $.each(list, function(key,value){
                    content_phong_tro += '<tr>';
                    content_phong_tro += '<td><a href="/customer/category/'+value.id_loai+'">'+ value.tieu_de +'</a></td>';
                    content_phong_tro += '</tr>';
                });
                $("#danhSachPhongTro tbody").html(content_phong_tro);
            }

            $("#searchPhongtro").keyup(function(){

                var search = $("#searchPhongtro").val();
                console.log(search);
                $payload = {
                    'search': search,
                };

                $.ajax({
                    url: '/customer/search',
                    type: 'post',
                    data: $payload,
                    success: function (res) {
                        viewPhongTro(res.listPhongTro);
                    }
                });
            });
        });
    </script>
</body>
</html>
