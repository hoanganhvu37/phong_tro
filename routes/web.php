<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/xxx', [\App\Http\Controllers\TestController::class, 'index']);
Route::get('/', [\App\Http\Controllers\HomeController::class, 'index']);

Route::group(['prefix' => '/admin','middleware' => 'AdminMiddleware'], function() {
    Route::group(['prefix' => '/loai-phong'], function() {
        Route::get('/index', [\App\Http\Controllers\LoaiPhongController::class, 'index']);
        Route::post('/index', [\App\Http\Controllers\LoaiPhongController::class, 'store']);
        Route::get('/get-data', [\App\Http\Controllers\LoaiPhongController::class, 'getData']);
        Route::get('/update-status/{id}', [\App\Http\Controllers\LoaiPhongController::class, 'updateStatus']);

        Route::get('/edit/{id}', [\App\Http\Controllers\LoaiPhongController::class, 'edit']);
        Route::post('/update', [\App\Http\Controllers\LoaiPhongController::class, 'update']);
        Route::get('/destroy/{id}', [\App\Http\Controllers\LoaiPhongController::class, 'destroy']);
    });

    Route::group(['prefix' => '/account'], function() {
        Route::get('/index', [\App\Http\Controllers\AdminController::class, 'index']);
        Route::post('/index', [\App\Http\Controllers\AdminController::class, 'store']);
        Route::post('/check-email', [\App\Http\Controllers\AdminController::class, 'checkemail']);
        Route::post('/sdt', [\App\Http\Controllers\AdminController::class, 'checksdt']);
        Route::post('/ma-admin', [\App\Http\Controllers\AdminController::class, 'checkmaadmin']);
        Route::get('/get-data', [\App\Http\Controllers\AdminController::class, 'getData']);
        Route::get('/update-status/{id}', [\App\Http\Controllers\AdminController::class, 'updateStatus']);
        Route::get('/edit/{id}', [\App\Http\Controllers\AdminController::class, 'edit']);
        Route::post('/update', [\App\Http\Controllers\AdminController::class, 'update']);
        Route::get('/destroy/{id}', [\App\Http\Controllers\AdminController::class, 'destroy']);
    });

    Route::group(['prefix' => '/chu-tro'], function() {
        Route::get('/index', [\App\Http\Controllers\ChuTroController::class, 'index']);
        Route::get('/get-data', [\App\Http\Controllers\ChuTroController::class, 'getData']);
        Route::get('/update-status/{id}', [\App\Http\Controllers\ChuTroController::class, 'updateStatus']);
        Route::get('/destroy/{id}', [\App\Http\Controllers\ChuTroController::class, 'destroy']);
    });

    Route::get('/logout', [\App\Http\Controllers\AdminController::class, 'logout']);
});

Route::group(['prefix' => '/chu-tro','middleware' => 'ChuTroMiddleware'], function() {

    Route::group(['prefix' => '/phong'], function() {
        Route::get('/index', [\App\Http\Controllers\ThemMoiPhongTroController::class, 'index']);
        Route::post('/index', [\App\Http\Controllers\ThemMoiPhongTroController::class, 'store']);
        Route::get('/get-data', [\App\Http\Controllers\ThemMoiPhongTroController::class, 'getData']);
        Route::get('/update-status/{id}', [\App\Http\Controllers\ThemMoiPhongTroController::class, 'updateStatus']);
        Route::get('/destroy/{id}', [\App\Http\Controllers\ThemMoiPhongTroController::class, 'destroy']);
        Route::get('/edit/{id}', [\App\Http\Controllers\ThemMoiPhongTroController::class, 'edit']);
        Route::post('/update', [\App\Http\Controllers\ThemMoiPhongTroController::class, 'update']);
        Route::post('/check-product-id', [\App\Http\Controllers\ThemMoiPhongTroController::class, 'checkProuctId']);
    });
    Route::group(['prefix' => '/payment'], function() {
        Route::get('/view-payment', [\App\Http\Controllers\ChuTroController::class, 'viewPayment']);
    });

    Route::group(['prefix' => '/us-chu-tro'], function() {
        Route::get('/index', [\App\Http\Controllers\ChuTroController::class, 'indexUpdate']);
        Route::get('/get-data', [\App\Http\Controllers\ChuTroController::class, 'getData']);
        Route::get('/update-status/{id}', [\App\Http\Controllers\ChuTroController::class, 'updateStatus']);
        Route::get('/edit', [\App\Http\Controllers\ChuTroController::class, 'edit']);
        Route::post('/update', [\App\Http\Controllers\ChuTroController::class, 'update']);
        Route::post('/change-password', [\App\Http\Controllers\ChuTroController::class, 'changePassword']);
    });

    Route::get('/logout', [\App\Http\Controllers\ChuTroController::class, 'logout']);
});

Route::group(['prefix' => '/customer','middleware' => 'CustomerMiddleware'], function() {
    Route::get('/favorite', [\App\Http\Controllers\YeuThichController::class, 'index']);
    Route::get('/add-yeu-thich/{id_phong}', [\App\Http\Controllers\YeuThichController::class, 'store']);
    Route::get('/delete-yeu-thich/{id_phong}', [\App\Http\Controllers\YeuThichController::class, 'destroyfavorite']);
    Route::get('/logout', [\App\Http\Controllers\CustomerController::class, 'logout']);
    Route::post('/bill-detail', [\App\Http\Controllers\BillController::class, 'store']);
    Route::get('/get-data', [\App\Http\Controllers\BillController::class, 'getData']);
    Route::get('/cart/{id}', [\App\Http\Controllers\BillController::class, 'payCart']);
    Route::get('/payment-status', [\App\Http\Controllers\BillController::class, 'paymentStatus']);
    Route::get('/delete-payment-status/{id}', [\App\Http\Controllers\BillController::class, 'deletePaymentStatus']);
});

Route::group(['prefix' => '/chu-tro'], function() {
    Route::get('/sign-up', [\App\Http\Controllers\ChuTroController::class, 'viewSignUp']);
    Route::post('/sign-up', [\App\Http\Controllers\ChuTroController::class, 'store']);
    Route::get('/login', [\App\Http\Controllers\ChuTroController::class, 'viewLogin']);
    Route::post('/login', [\App\Http\Controllers\ChuTroController::class, 'actionLogin']);
    Route::get('/kich-hoat/{hash}', [\App\Http\Controllers\ChuTroController::class, 'active']);
});

Route::group(['prefix' => '/customer'], function() {
    Route::get('/product/{id}', [\App\Http\Controllers\HomeController::class, 'detailProduct']);
    Route::get('/category/{id}', [\App\Http\Controllers\HomeController::class, 'listProduct']);
    Route::get('/index', [\App\Http\Controllers\CustomerController::class, 'index']);
    Route::get('/view-signup', [\App\Http\Controllers\CustomerController::class, 'viewSignUp']);
    Route::post('/sign-up', [\App\Http\Controllers\CustomerController::class, 'store']);
    Route::get('/view-login', [\App\Http\Controllers\CustomerController::class, 'viewLogin']);
    Route::post('/login', [\App\Http\Controllers\CustomerController::class, 'actionLogin']);
    Route::post('/search', [\App\Http\Controllers\HomeController::class, 'search']);
    Route::get('/kich-hoat/{hash}', [\App\Http\Controllers\CustomerController::class, 'active']);
});
Route::get('/admin/login', [\App\Http\Controllers\AdminController::class, 'viewLogin']);
Route::post('/admin/login', [\App\Http\Controllers\AdminController::class, 'actionLogin']);


Route::post('/123', [\App\Http\Controllers\AdminController::class, '123']);

Route::group(['prefix' => 'laravel-filemanager',], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});












