<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable
{
    use HasFactory;

    protected $table = 'customers';

    protected $fillable = [
        'ho_lot',
        'ten',
        'phone',
        'email',
        'sex',
        'dob',
        'password',
        'is_active',
        'is_block',
        'is_yeuthich',
        'hash',
        'hash_reset',
        'real_email',
    ];
}
