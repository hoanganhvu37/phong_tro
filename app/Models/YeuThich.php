<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class YeuThich extends Model
{
    use HasFactory;

    protected $table  = 'yeu_thiches';
    protected $fillable = [
        'id_phong',
        'tieu_de_phong',
        'hinh_anh',
        'don_gia_thang',
        'mo_ta_ngan_phong',
        'customer_id',
    ];
}
