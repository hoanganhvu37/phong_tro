<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ChuTro extends Authenticatable
{
    use HasFactory;

    protected $table = 'chu_tros';

    protected $fillable = [
        'ma_chu_tro',
        'ho_lot',
        'ten',
        'email',
        'ngay_sinh',
        'gioi_tinh',
        'cmnd_cccd',
        'dia_chi',
        'password',
        'so_dien_thoai',
        'is_block',
        'is_active',
        'is_email',
        'hash',
        'hash_reset',
        'hinh_anh',
    ];
}
