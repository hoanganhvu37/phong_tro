<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ThemMoiPhongTro extends Model
{
    use HasFactory;

    protected $table  = 'them_moi_phong_tros';
    protected $fillable = [
        'ma_phong_tro',
        'id_loai',
        'tieu_de',
        'slug_tieu_de',
        'dia_chi_phong_tro',
        'id_chu_tro',
        'so_phong',
        'is_open',
        'hinh_anh',
        'gia_thang',
        'thoi_gian_coc',
        'tien_coc',
        'mo_ta_phong_tro',
        'mo_ta_chi_tiet',
    ];
}
