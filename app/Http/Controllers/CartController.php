<?php

namespace App\Http\Controllers;

use App\Models\ThemMoiPhongTro;
use App\Models\ThueTroCustomer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    public function index()
    {
        $customer = Auth::guard('customer')->user();

        $thuTro = ThueTroCustomer::where('id_customer',$customer->id)
                                ->get();
        // dd($thuTro);



        return view('client.page.cart', compact('thuTro'));
    }
}
