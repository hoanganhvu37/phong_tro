<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoaiPhong\CreateLoaiPhongRequest;
use App\Http\Requests\LoaiPhong\UpdateLoaiPhongRequest;
use App\Models\LoaiPhong;
use Illuminate\Http\Request;

class LoaiPhongController extends Controller
{

    public function index()
    {
        return view('admin.pages.loai_phong.index');
    }

    public function getData()
    {
        $loaiPhong    = LoaiPhong::all();
        // dd($loaiPhong);
        return response()->json([
            'data'      => $loaiPhong,
        ]);
    }
    public function updateStatus($id)
    {
        $loaiPhong = LoaiPhong::find($id);
        if ($loaiPhong){
            $loaiPhong->is_open = !$loaiPhong->is_open;
            $loaiPhong->save();

            return response()->json([
                'status' => true,
            ]);
        }else {
            return response()->json([
                'status' => false,
            ]);
        }

    }

    public function store(CreateLoaiPhongRequest $request)
    {
        $data = $request->all();
        LoaiPhong::create($data);
        return response()->json([
            'status'    => 1,
        ]);
    }

    public function edit($id)
    {
        $loaiPhong = LoaiPhong::find($id);  // trả về 1 object
        if ($loaiPhong) {
            return response()->json([
                'status'    => true,
                'data'      => $loaiPhong,
            ]);
        } else {
            return response()->json([
                'status'    => false,
            ]);
        }
    }

    public function update(UpdateLoaiPhongRequest $request)
    {
        $loaiPhong = LoaiPhong::find($request->id);
        $data = $request->all();
        $loaiPhong->update($data);
    }

    public function destroy($id)
    {
        $loaiPhong = LoaiPhong::find($id);
        if ($loaiPhong) {
            $loaiPhong->delete();

            return response()->json([
                'status'  => true,
            ]);
        } else {
            return response()->json([
                'status'  => false,
            ]);
        }
    }
}
