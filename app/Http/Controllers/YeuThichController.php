<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\ThemMoiPhongTro;
use App\Models\YeuThich;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class YeuThichController extends Controller
{
    public function index()
    {
        $customer = Auth::guard('customer')->user();
        // dd($customer);
        $list = ThemMoiPhongTro::join('yeu_thiches', 'them_moi_phong_tros.id', 'yeu_thiches.id_phong')
                                ->join('customers', 'yeu_thiches.customer_id', 'customers.id')
                                ->where('them_moi_phong_tros.is_open', 1)
                                ->where('yeu_thiches.customer_id', $customer->id)
                                ->select('them_moi_phong_tros.*','yeu_thiches.id_phong','yeu_thiches.customer_id', 'customers.id')
                                ->get();
        // dd($list->toArray());
        return view('client.page.yeuthich',compact('list'));
    }

    public function store($id_phong)
    {
        $customer = Auth::guard('customer')->user();
        // dd(123);
        $phongTro = ThemMoiPhongTro::find($id_phong);

        if($phongTro){
            $checkYeuThich = YeuThich::where('id_phong', $id_phong)
                                    ->where('customer_id',$customer->id)
                                    ->first();
            if($checkYeuThich){
                $checkYeuThich->delete();
                $checkYeuThich->save();
                return response()->json([
                    'status'   =>   1,
                    'message'   =>  'Phòng trọ đã thêm vào yêu thích trước đó',
                ]);
            } else {
                YeuThich::create([
                    'id_phong' =>   $id_phong,
                    'customer_id' => $customer->id,
                ]);
            }
            return response()->json([
                'status'   =>   0,
                'message'   =>  'Đã Thêm Vào Yêu Thích',
            ]);
        }else{
            YeuThich::where('id_phong', $id_phong)->delete();

            return response()->json([
                'status'    =>   2,
                'message'   =>  'Phòng Trọ Không Tồn Tại',
            ]);
        }

    }

    public function destroyfavorite($id)
    {
        $customer = Auth::guard('customer')->user();
        // dd(123);
        $phongTro = YeuThich::where('id_phong',$id)
                            ->first();
        // dd($phongTro);
        if($phongTro){
            $phongTro->delete();
            return response()->json([
                'status'   =>   true,
                'message'   =>  'Đã bỏ yêu thích',
            ]);
        }else{
            return response()->json([
                'status'   =>   false,
                'message'   =>  'Có lỗi xảy ra',
            ]);
        }
    }
}
