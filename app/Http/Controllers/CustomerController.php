<?php

namespace App\Http\Controllers;

use App\Http\Requests\Customer\CreateCustomerRequest;
use App\Http\Requests\Customer\LoginRequest;
use App\Http\Requests\ThueTro\ThueTroCustomerRequest;
use App\Jobs\sendMailActiveCustomerJob;
use Illuminate\Http\Request;
use App\Jobs\sendMailActiveJob;
use App\Mail\CustomerAcitveMail;
use App\Models\Customer;
use App\Models\ThemMoiPhongTro;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;


class CustomerController extends Controller
{
    public function index()
    {
        return view('client.master');
    }

    public function viewSignUp()
    {
        return view('client.page.sign_up');
    }

    public function viewLogin()
    {
        return view('client.page.login');
    }

    public function store(CreateCustomerRequest $request)
    {
        // dd($request->all());

        $hash = Str::uuid()->toString();
        $data = $request->all();
        $data['password'] = bcrypt($request->password);
        $data['hash'] = $hash;
        $link = env('APP_URL') . '/customer/kich-hoat/' . $hash;
        $parts = explode(" ", $request->full_name);

        if(count($parts) > 1) {
            $lastname = array_pop($parts);
            $firstname = implode(" ", $parts);
        }
        else
        {
            $firstname = $request->full_name;
            $lastname = " ";
        }

        $data['ho_lot']     = $firstname;
        $data['ten']        = $lastname;
        $data['password']   = bcrypt($request->password);

        sendMailActiveCustomerJob::dispatch($request->full_name, $link, $request->email);

        Customer::create($data);

        return response()->json([
            'status' => $data,
        ]);
    }

    public function actionLogin(LoginRequest $request)
    {
        $data = $request->only('email','password');
        // dd($data);
        $login = Auth::guard('customer')->attempt($data);

        if($login){
            $customer = Auth::guard('customer')->user();
            if($customer->is_active == 1) {
                return response()->json(['status' => 4]);
            }else{
                Auth::guard('chu_tro')->logout();
                return response()->json(['status' => 5]);
            }
        }else{
            return response()->json(['status' => 0]);
        }
    }

    public function active($hash)
    {
        $customer = Customer::where('hash', $hash)->first();
        if($customer) {
            if($customer->is_active == 1){
                toastr()->warning("Tài Khoản Đã Được Kích Hoạt Trước Đó");
            }else{
                toastr()->success("Email " . $customer->email . " đã được kích hoạt");
                $customer->is_active = 1;
                $customer->save();
            }
        }else {
            toastr()->error("Mã kích hoạt không tồn tại");
        }
        return redirect('/');
    }

    public function logout()
    {
        Auth::guard('customer')->logout();
        return redirect('/');
    }


}
