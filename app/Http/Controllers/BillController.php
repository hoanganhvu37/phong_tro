<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\ThemMoiPhongTro;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Bill\BillRequest;
use App\Models\Bill;
use App\Models\Bill_detail;
use App\Models\ChuTro;

class BillController extends Controller
{

    // public function getData()
    // {
    //     $user = Auth::guard('customer')->user();

    //     $customer = Customer::where('id',$user->id)->first();
    //     // dd($customer->toArray());
    //     $phongTro = ThemMoiPhongTro::where('is_open',1)->first();
    //     // dd($phongTro->toArray());
    //     return response()->json([
    //         'status'    => true,
    //         'data'      => $customer,
    //         'phongTro'  => $phongTro,
    //     ]);
    // }

    public function payCart($id)
    {
        $user = Auth::guard('customer')->user();
        $customer = Customer::where('id',$user->id)->first();

        $phongTro = ThemMoiPhongTro::join('loai_phongs', 'them_moi_phong_tros.id_loai', 'loai_phongs.id')
                                    ->where('them_moi_phong_tros.id', $id)
                                    ->select('them_moi_phong_tros.*', 'loai_phongs.ten_loai_phong')
                                    ->first();
        // dd($customer->toArray(),$phongTro->toArray());
        return view('client.page.cart',compact('customer','phongTro'));
    }

    public function store(BillRequest $request)
    {
        $customer = Auth::guard('customer')->user();

            $data = $request->all();
            $check = Bill_detail::where('id_customer', $customer->id)
                                ->where('id_done',0)
                                ->first();
            if($check){
                $check->delete();
                $check->save();
                return response()->json(['status' => false]);
            }else{
                Bill_detail::create($data);
                return response()->json(['status' => true]);
            }
    }

    public function paymentStatus()
    {
        $customer = Auth::guard('customer')->user();

        $sttPayment = Bill_detail::join('them_moi_phong_tros','bill_details.id_phong','them_moi_phong_tros.id')
                                ->select('them_moi_phong_tros.*','bill_details.id_done','bill_details.id')
                                ->where('bill_details.id_customer', $customer->id)
                                ->get();

        // dd($sttPayment->toArray());

        return view('client.page.payment_status',compact('sttPayment'));
    }

    public function deletePaymentStatus($id)
    {
        $sttPayment = Bill_detail::find($id);
        if($sttPayment) {
            $sttPayment->delete();

            return response()->json([
                'status'  => true,
            ]);
        } else {
            return response()->json([
                'status'  => false,
            ]);
        }
    }
}
