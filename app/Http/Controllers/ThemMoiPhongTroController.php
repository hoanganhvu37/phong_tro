<?php

namespace App\Http\Controllers;

use App\Http\Requests\ThemMoiPhongTro\CreateThemMoiPhongTroRequest;
use App\Http\Requests\ThemMoiPhongTro\UpdateThemMoiPhongTroRequest;
use App\Models\ChuTro;
use App\Models\LoaiPhong;
use App\Models\ThemMoiPhongTro;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ThemMoiPhongTroController extends Controller
{

    public function index()
    {
        return view('chu_tro.pages.phong_tro.index');
    }


    public function getData()
    {
        $chuTro = Auth::guard('chu_tro')->user();
        // dd($chuTro);
        $themMoiPhongTro = ThemMoiPhongTro::join('loai_phongs', 'them_moi_phong_tros.id_loai', 'loai_phongs.id')
                       ->where('id_chu_tro', $chuTro->id)
                       ->select('them_moi_phong_tros.*', 'loai_phongs.ten_loai_phong')
                       ->get();
        // dd($themMoiPhongTro->toArray());
        $loaiPhong = LoaiPhong::where('is_open', 1)->get();
        return response()->json([
            'data'  => $themMoiPhongTro,
            'listLoaiPhong' => $loaiPhong
        ]);
    }


    public function store(CreateThemMoiPhongTroRequest $request)
    {
        $chuTro = Auth::guard('chu_tro')->user();
        $data = $request->all();
        $data['id_chu_tro'] = $chuTro->id;
        $data['tien_coc'] = $request->gia_thang * 0.1;
        ThemMoiPhongTro::create($data);
        return response()->json([
            'status' => true,
            'mess' => 'Thêm mới phòng trọ thành công !',
        ]);
    }

    public function updateStatus($id)
    {
        $themMoiPhongTro = ThemMoiPhongTro::find($id);
        if ($themMoiPhongTro){
            $themMoiPhongTro->is_open = !$themMoiPhongTro->is_open;
            $themMoiPhongTro->save();

            return response()->json([
                'status' => true,
            ]);
        }else {
            return response()->json([
                'status' => false,
            ]);
        }

    }

    public function destroy($id)
    {
        $themMoiPhongTro = ThemMoiPhongTro::find($id);
        if ($themMoiPhongTro) {
            $themMoiPhongTro->delete();

            return response()->json([
                'status'  => true,
            ]);
        } else {
            return response()->json([
                'status'  => false,
            ]);
        }
    }

    public function edit($id)
    {
        $themMoiPhongTro = ThemMoiPhongTro::find($id);  // trả về 1 object
        if ($themMoiPhongTro) {
            return response()->json([
                'status'    => true,
                'data'      => $themMoiPhongTro,
            ]);
        } else {
            return response()->json([
                'status'    => false,
            ]);
        }
    }


    public function update(UpdateThemMoiPhongTroRequest $request)
    {
        $themMoiPhongTro = ThemMoiPhongTro::find($request->id);
        $data = $request->all();
        $data['tien_coc'] = $data['gia_thang'] * 0.1;
        $themMoiPhongTro->update($data);
        return response()->json([
            'status' => true,
            'mess'   => 'Cập nhật phòng trọ thành công !',
        ]);
    }

    public function checkProuctId(Request $request)
    {
        $themMoiPhongTro = ThemMoiPhongTro::where('ma_phong_tro',$request->ma_phong_tro)->first();

        if($themMoiPhongTro){
            return response()->json(['status' => true]);
        }else{
            return response()->json(['status' => false]);
        }
    }

}
