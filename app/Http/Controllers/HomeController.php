<?php

namespace App\Http\Controllers;

use App\Models\ChuTro;
use App\Models\ThemMoiPhongTro;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    {

        // $chuTro = Auth::guard('chu_tro')->user();
        $chuTro = ChuTro::where('is_block', 0)->select('chu_tros.id')->get();

        // dd($chuTro);

        $phongTro = ThemMoiPhongTro::orderBy('id')
                                    ->where('is_open', 1)
                                    ->whereIn('id_chu_tro', $chuTro)
                                    ->get();

        // dd($phongTro);

        return view('client.page.home', compact('phongTro'));

    }

    public function detailProduct($id)
    {
        $phongTro = ThemMoiPhongTro::join('loai_phongs', 'them_moi_phong_tros.id_loai', 'loai_phongs.id')
                            ->where('them_moi_phong_tros.id', $id)
                            ->select('them_moi_phong_tros.*', 'loai_phongs.ten_loai_phong')
                            ->first();

        $phongTroMoi = ThemMoiPhongTro::orderBy('id')
                                    ->where('is_open', 1)
                                    ->get();
        //  dd($phongTro, $phongTroMoi);

        return view('client.page.product_detail', compact('phongTro','phongTroMoi'));
    }

    public function listProduct($id)
    {
        $listPhongTro = ThemMoiPhongTro::join('loai_phongs', 'them_moi_phong_tros.id_loai', 'loai_phongs.id')
                                    ->where('them_moi_phong_tros.id_loai', $id)
                                    ->where('them_moi_phong_tros.is_open', 1)
                                    ->select('them_moi_phong_tros.*','loai_phongs.ten_loai_phong')
                                    ->get();

        // dd($phongTro->toArray());

        return view('client.page.product_loaiphong_detail', compact('listPhongTro'));
    }

    public function search(Request $request)
    {
        $search = $request->search;
        $data= ThemMoiPhongTro::where('tieu_de', 'like', '%'.$search.'%')
                            ->get();
        dd($data);
        return response()->json([
            'listPhongTro' => $data,
        ]);
    }
}
