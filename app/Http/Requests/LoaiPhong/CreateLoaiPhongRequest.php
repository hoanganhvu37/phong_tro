<?php

namespace App\Http\Requests\LoaiPhong;

use Illuminate\Foundation\Http\FormRequest;

class CreateLoaiPhongRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'ten_loai_phong'                => 'required|min:5',
            'slug_loai_phong'               => 'required|min:5',
            'is_open'                       => 'required|boolean',
        ];
    }

    public function messages()
    {
        return [
           'ten_loai_phong.required'         => 'Tên loại phòng không thể để trống',
           'slug_loai_phong.required'        => 'Slug loại phòng không thể để trống',
           'is_open.required'                => 'Tình trạng không thể để trống',
           'ten_loai_phong.min'              => 'Tên loại phòng phải từ 5 ký tự trở lên',
           'slug_loai_phong.min'             => 'Slug loại phòng phải từ 5 ký tự trở lên',
           'is_open.boolean'                 => 'Tình trạng chỉ được chọn Yes/No',
        ];
    }
}
