<?php

namespace App\Http\Requests\Customer;

use Illuminate\Foundation\Http\FormRequest;

class CreateCustomerRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'full_name'             =>      'required|min:5',
            'phone'                 =>      'required|digits:10|unique:customers,phone',
            'email'                 =>      'required|email|unique:customers,email',
            'sex'                   =>      'required|boolean',
            'dob'                   =>      'required|date',
            'password'              =>      'required|min:5|max:30',
            're_password'           =>      'required|same:password',
        ];
    }
}
