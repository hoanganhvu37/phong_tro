<?php

namespace App\Http\Requests\Customer;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'email'                 => 'required|email',
            'password'              => 'required',
            'g-recaptcha-response'  => 'required|captcha',
        ];
    }
}
