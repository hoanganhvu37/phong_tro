<?php

namespace App\Http\Requests\ThemMoiPhongTro;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class CreateThemMoiPhongTroRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'ma_phong_tro'                  => 'required|min:5|unique:them_moi_phong_tros,ma_phong_tro',
            'tieu_de'                       => 'required|min:5|unique:them_moi_phong_tros,tieu_de',
            'slug_tieu_de'                  => 'required|min:5|unique:them_moi_phong_tros,slug_tieu_de',
            'id_loai'                       => 'required|exists:loai_phongs,id',
            'dia_chi_phong_tro'             => 'required|min:5',
            'so_phong'                      => 'required|numeric|min:1',
            'is_open'                       => 'required|boolean',
            'hinh_anh'                      => 'required',
            'gia_thang'                     => 'required|numeric|min:1000',
            'thoi_gian_coc'                 => 'required|numeric|min:1',
            'tien_coc'                      => 'nullable|numeric|min:10000',
            'mo_ta_phong_tro'               => 'required|min:5',
            'mo_ta_chi_tiet'                => 'required|min:5',
        ];
    }

    public function messages()
    {
        return [
            'required'      =>  ':attribute không được để trống',
            'min'           =>  ':attribute quá nhỏ/ngắn',
            'boolean'       =>  ':attribute không phải Yes/No',
            'numeric'       =>  ':attribute không phải là số',
            'exists'        =>  ':attribute không tồn tại',
            'unique'        =>  ':attribute đã tồn tại trong hệ thống',
        ];
    }

    public function attributes()
    {
        return [
            'ma_phong_tro'                  => 'Mã Phòng Trọ',
            'tieu_de'                       => 'Tiêu Đề',
            'slug_tieu_de'                  => 'Slug Tiêu Đề',
            'id_loai'                       => 'Loại Phòng',
            'dia_chi_phong_tro'             => 'Địa Chỉ Phòng Trọ',
            'so_phong'                      => 'Số Phòng',
            'is_open'                       => 'Tình Trạng',
            'hinh_anh'                      => 'Hình Ảnh',
            'gia_thang'                     => 'Giá Tháng',
            'mo_ta_phong_tro'               => 'Mô Tả Phòng Trọ',
            'mo_ta_chi_tiet'                => 'Mô Tả Chi Tiết',
            'thoi_gian_coc'                 => 'Thời gian cọc',
        ];
    }
}
