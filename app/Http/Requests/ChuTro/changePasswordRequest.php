<?php

namespace App\Http\Requests\ChuTro;

use Illuminate\Foundation\Http\FormRequest;

class changePasswordRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'password'              =>      'required|min:5|max:30',
            're_password'           =>      'required|same:password',
        ];
    }
}
