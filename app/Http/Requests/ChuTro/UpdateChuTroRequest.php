<?php

namespace App\Http\Requests\ChuTro;

use Illuminate\Foundation\Http\FormRequest;

class UpdateChuTroRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'full_name'             =>      'required|min:5',
            'so_dien_thoai'         =>      'required|digits:10' . $this->id,
            'email'                 =>      'required|email' . $this->id,
            'cmnd_cccd'             =>      'nullable|digits_between:10,12',
            'gioi_tinh'             =>      'nullable|min:0|max:1',
            'dia_chi'               =>      'nullable|min:5',
            'ngay_sinh'             =>      'nullable|date',
        ];
    }
}
