<?php

namespace App\Http\Requests\ChuTro;

use Illuminate\Foundation\Http\FormRequest;

class CreateDangKyChuTroRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'full_name'             =>      'required|min:5',
            'so_dien_thoai'         =>      'required|digits:10|unique:chu_tros,so_dien_thoai',
            'email'                 =>      'required|email|unique:chu_tros,email',
            'cmnd_cccd'             =>      'digits_between:10,12',
            'dia_chi'               =>      'min:5',
            'password'              =>      'required|min:5|max:30',
            're_password'           =>      'required|same:password',
        ];
    }
}
