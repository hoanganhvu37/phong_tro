<?php

namespace App\Http\Requests\Bill;

use Illuminate\Foundation\Http\FormRequest;

class BillRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'address'                    =>      'required|min:5|max:50',
            'id_customer'                =>      'required',
            'id_phong'                   =>      'required',
        ];
    }
}
