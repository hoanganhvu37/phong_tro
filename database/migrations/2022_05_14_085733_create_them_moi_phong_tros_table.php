<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{

    public function up()
    {
        Schema::create('them_moi_phong_tros', function (Blueprint $table) {
            $table->id();
            $table->string('ma_phong_tro');
            $table->integer('id_loai');
            $table->string('tieu_de');
            $table->string('slug_tieu_de');
            $table->longText('dia_chi_phong_tro');
            $table->integer('id_chu_tro');
            $table->string('so_phong');
            $table->integer('is_open');
            $table->string('hinh_anh');
            $table->double('tien_coc');
            $table->integer('thoi_gian_coc');
            $table->double('gia_thang', 18, 0);
            $table->longText('mo_ta_phong_tro');
            $table->longText('mo_ta_chi_tiet');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('them_moi_phong_tros');
    }
};
