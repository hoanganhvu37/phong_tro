<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bill_details', function (Blueprint $table) {
            $table->id();
            $table->integer('id_bill')->nullable();
            $table->integer('id_phong');
            $table->integer('id_customer');
            $table->string('bill_name')->nullable();
            $table->double('deposit')->nullable();
            $table->double('deposit_day')->nullable();
            $table->string('address')->nullable();
            $table->double('gia_thue', 18, 0)->nullable();
            $table->integer('id_done')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bill_details');
    }
};
