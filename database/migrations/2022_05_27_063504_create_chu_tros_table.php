<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chu_tros', function (Blueprint $table) {
            $table->id();
            $table->string('ma_chu_tro')->nullable();
            $table->string('ho_lot');
            $table->string('ten');
            $table->string('email')->unique();
            $table->string('cmnd_cccd')->nullable()->unique();
            $table->string('dia_chi')->nullable();
            $table->date('ngay_sinh')->nullable();
            $table->integer('gioi_tinh')->default(1);
            $table->string('password');
            $table->string('so_dien_thoai')->unique();
            $table->integer('is_block')->default(0);
            $table->integer('is_active')->default(0);
            $table->string('hash')->nullable();
            $table->string('hash_reset')->nullable();
            $table->string('hinh_anh')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chu_tros');
    }
};
